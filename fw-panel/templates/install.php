<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $page->title; ?></title>
	<!--                       CSS                       -->
	<!-- Reset Stylesheet -->
	<link rel="stylesheet" href="<?php echo PANEL_URL; ?>resources/css/reset.css" type="text/css" media="screen" />
	<!-- Main Stylesheet -->
	<link rel="stylesheet" href="<?php echo PANEL_URL; ?>resources/css/style.css" type="text/css" media="screen" />
	<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
	<link rel="stylesheet" href="<?php echo PANEL_URL; ?>resources/css/invalid.css" type="text/css" media="screen" />
	<!-- Colour Schemes
		  
	Default colour scheme is green. Uncomment prefered stylesheet to use it.
	<link rel="stylesheet" href="resources/css/blue.css" type="text/css" media="screen" />		
	<link rel="stylesheet" href="resources/css/red.css" type="text/css" media="screen" />  
	
	-->
	<!-- Internet Explorer Fixes Stylesheet -->
	<!--[if lte IE 7]>
				<link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
			<![endif]-->
	<!--                       Javascripts                       -->
	<!-- jQuery -->
	<script type="text/javascript" src="<?php echo PANEL_URL; ?>resources/js/jquery-1.4.1.min.js"></script>
	<!-- jQuery Configuration -->
	<script type="text/javascript" src="<?php echo PANEL_URL; ?>resources/js/simpla.jquery.configuration.js"></script>
	<!-- Facebox jQuery Plugin -->
	<script type="text/javascript" src="<?php echo PANEL_URL; ?>resources/js/facebox.js"></script>
	<script type="text/javascript" src="<?php echo PANEL_URL; ?>resources/js/ajax.js"></script>
    <script type="text/javascript" src="<?php echo PANEL_URL; ?>resources/js/functions.js"></script>
    <!--<script type="text/javascript" src="<?php echo PANEL_URL; ?>resources/js/freshwork.js"></script>-->
	<!-- jQuery WYSIWYG Plugin -->
	<script type="text/javascript" src="<?php echo PANEL_URL; ?>resources/js/jquery.wysiwyg.js"></script>
	<!-- Internet Explorer .png-fix -->
	<!--[if IE 6]>
		<script type="text/javascript" src="resources/js/DD_belatedPNG_0.0.7a.js"></script>
		<script type="text/javascript">
			DD_belatedPNG.fix('.png_bg, img, li');
		</script>
	<![endif]-->
	<?php echo $page->head; ?>
</head>
<body id="login">
	<?php echo $page->body; ?>
<!-- End #login-wrapper -->
</body>
</html>
