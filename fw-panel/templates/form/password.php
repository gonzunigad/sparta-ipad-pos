<div class="input text <?php if(isset($options["class"]))echo $options["class"]; ?>">
	<label for="<?php echo $options["code"]; ?>"><?php echo $options["label"]; ?></label>
	<input type="password" name="<?php echo $options["name"]; ?>" id="<?php echo $options["code"]; ?>" value="<?php if(isset($options["value"]))echo ($options["value"]); ?>" />
	<?php if(isset($options["error"]) && $options["error"] != ""){ ?>
		<span class="error"><?php echo $options["error"]; ?></span>
	<?php } ?>
	<?php if(isset($options["desc"]) && $options["desc"] != ""){ ?>
		<small><?php echo $options["desc"]; ?></small>
	<?php } ?>
</div>