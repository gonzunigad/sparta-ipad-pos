<div class="input textarea">
	<label for="<?php echo $options["code"]; ?>"><?php echo $options["label"]; ?></label>
	<textarea name="<?php echo $options["name"]; ?>" id="<?php echo $options["code"]; ?>"><?php if(isset($options["value"]))echo ($options["value"]); ?></textarea>
	<?php if(isset($options["error"]) && $options["error"] != ""){ ?>
		<span class="error"><?php echo $options["error"]; ?></span>
	<?php } ?>
	<?php if(isset($options["desc"]) && $options["desc"] != ""){ ?>
		<small><?php echo $options["desc"]; ?></small>
	<?php } ?>
</div>