<?php
if(isset($_POST['user']) && isset($_POST['pwd'])){
	$user = Model::instance("User");
	$userdata = $user->login($_POST["user"],$_POST["pwd"]);
	if($userdata){ // Si los datos de acceso son correctos...
		$_SESSION['usr'] = $userdata;
		$goto = (isset($_GET['u']) && $_GET['u'] != "")?$_GET['u']:PANEL_URL;
		if($_POST['goto'] != "")$goto .= "#!".$_POST['goto'];
		header("Location: $goto");
		exit;
	}else{
		$html_message = Component::get("msgbox",array("type" => "error","msg" => __("Los datos de acceso son incorrectos")));
	}
}
?>