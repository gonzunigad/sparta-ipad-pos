<?php 
$page->template = "install";
if(isset($_GET['shadowbox']))$this->template = "shadowbox"; ?>
<script type="text/javascript">
    $(function(){
       $("#test_connection").click(function(){
           $.post("fw-files/standalone/try.connect.php",$("form").serialize(),function(resp){
               var resp = eval("("+resp+")");
               if(resp.estado == 'cant_connect'){
                    addMessage("La contraseña no funciona.","error","#login-content",false);
               }else if(resp.estado == 'cant_select'){
                   addMessage("No se puede seleccionar la BD.","error","#login-content",false);
               }else{
                   addMessage("Conectado satisfacotriamente. Los datos están OK.","success","#login-content",false);
               }
           });
       });
    });
</script>
<?php PB::endhead(); ?>
<div id="login-wrapper" class="png_bg">
	<div id="login-top">
		<h1><?php ___("Instalación de FreshWork"); ?></h1>
		<!-- Logo (221px width) -->
		<p style="font-size:12px;">&iquest;A&uacute;n no creas tu archivo de configuraci&oacute;n? Nosotros lo hacemos por t&iacute;.</p>
	</div>
	<!-- End #logn-top -->
	<div id="login-content">
		<div class="notification information png_bg">
			<div>Puedes instalar Freshwork manualmente.<br /><a href="#;">M&aacute;s informaci&oacute;n....</a> </div>
		</div>
		<?php 
		if(isset($_GET['m'])){
		switch($_GET['m']){
			case "cant_connect": ?>
				<div class="notification error png_bg">
					<div>Tu archivo de configuraci&oacute;n ya est&aacute; creado, pero a&uacute;n as&iacute; no puedo conectarme a la base de datos. <strong style="color:#fff">Por favor, vuelve a ingresar los datos de acceso.</strong></div>
				</div>
				<?php break;
			case "cant_select_db":?>
				<div class="notification error png_bg">
					<div>Tu archivo de configuraci&oacute;n ya est&aacute; creado, pero a&uacute;n así no puedo conectarme a la base de datos.<br />Por favor, vuelve a ingresar los datos de acceso.</div>
				</div>
				<?php break;
			} 
		}?>
		<form action="" method="post" class="form one_line">
			<?php if($paso == 1){ ?>
			<h3 style="color:#fff;">Base de datos</h3>
			<fieldset>
				<input type="hidden" name="paso" value="1" />
				<!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
				<div class="input text">
					<label>Servidor: </label>
					<input value="localhost" type="text" value="<?php echo $conf["DB.HOST"]; ?>"  name="dbhost" />
				</div>
				<div class="input text">
					<label>Nombre: </label>
					<input class="text-input medium-input" value="<?php echo $conf["DB.NAME"]; ?>" type="text" name="dbname" />
				</div>
				<div class="input text">
					<label>Usuario: </label>
					<input class="text-input medium-input" value="<?php echo $conf["DB.USER"]; ?>" type="text" name="dbuser" />
				</div>
				<div class="input text">
					<label><?php ___("Contraseña:"); ?> </label>
					<input class="text-input medium-input" type="password" name="dbpass" />
				</div>
			</fieldset>

            <p>
				<input class="button" id="test_connection" type="button" value="<?php ___("Probar conexión"); ?>" style="float:left;" />
			</p>
			<?php }elseif($paso == 2){ ?>
			<h3 style="color:#fff">Datos de Acceso FTP</h3>
			<fieldset>
				<input type="hidden" name="paso" value="2" />
				<!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
				<div class="input text">
					<label>Usuario: </label>
					<input class="text-input medium-input" type="text" name="ftpuser" />
                </div>
				<div class="clear"></div>
				<div class="input text">
					<label>Contrase&ntilde;a: </label>
					<input class="text-input medium-input" type="password" name="ftppass" />
                </div>
			</fieldset>
			<?php } ?>
			<p>
				<input class="button" type="submit" value="Continuar instalaci&oacute;n..." />
			</p>
			<div class="clear"></div>
		</form>
	</div>
	<!-- End #login-content -->
</div>