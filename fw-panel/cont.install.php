<?php
$paso = 1;
if(count($_POST) > 0){
	$permisos = substr(sprintf('%o', fileperms(FILES_DIR)), -4);
	if($_POST['paso'] == 1){
        $_SESSION['install.content'] = '
$conf["INSTALLED"]      = false;';
		$_SESSION['install.content'] .= '
$conf["DB.TYPE"] 		= "mysql";
$conf["DB.HOST"] 		= "'.$_POST['dbhost'].'";
$conf["DB.NAME"]		= "'.$_POST['dbname'].'";
$conf["DB.USER"] 		= "'.$_POST['dbuser'].'";
$conf["DB.PWD"]			= "'.$_POST['dbpass'].'";';
	}else{
		$_SESSION['install.content'] .= '
$conf["FTP.USER"] 		= "'.$_POST['ftpuser'].'";
$conf["FTP.PWD"]		= "'.$_POST['ftppass'].'";';
    }
	
	if(($_POST['paso'] == 1 && $permisos == '0777') || $_POST['paso'] == 2){
		file_put_contents(CONFIG_PATH,'<?php '.$_SESSION['install.content']."\n".'?>');
        include(CONFIG_PATH);	
        require(INCS_DIR."connect.db.php");
        require(INCS_DIR."install.php");
		header("Location: index.php");
	}else{
		$paso = 2;	
	}
}

?>