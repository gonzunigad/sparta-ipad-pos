<?php
//i'm a web page
class Page extends MainPage{
	static $instance = null;
	var $body;
	var $head;
	var $title;
	var $file;
	var $pageCode = false;
	var $url; //URL Solicitada por el cliente
	var $rel_url;
	var $complete_url;
	var $module_url; // URL relativa al módulo: Ej: /fw-panel/products/new -> new 
	var $ts_inicio;
	var $ts_fin;
	var $rendered; 
	var $content; //Casi al finalizar todos los procesos, esta variable contiene todo el HTML que se imprimirá.
	var $printed; //Indica si el HTML contenido en la variable $content fué o no impreso. 
	var $standAlone; 
	var $javascripts = array();
	var $charset; //Indica el charset que se cargará en el HEAD del HTML
	var $favicon;
	var $is_ajax; //Indica si la URL actual fué o no solicitada mediante AJAX.
	var $template_dir; //Espcifíca el directorio en donde se buscaran los archivos que se utilizaran como layout.
	var $template; //Especifíca que archivo que este dentro de la carpeta $template_dir se cargara como layout.
	var $is_module; //Indica si se esta trabajando sobre un archivo de algún módulo o no
	var $current_module = ""; //Especifíca sobre que modulo se esta trabajando
	var $load_ajax_js; //Especifica si se va a cargar el script ajax.reload.js dentro del HTML
	var $put_additional_content;
    var $event_listeners = array();
	function Page(){
		$this->file = $_SERVER['PHP_SELF'];
		$this->rel_url = $_GET['url'];
		$this->url = ABS_URL.$this->rel_url;
		$this->title = $_SERVER['HTTP_HOST'];
		$this->template = "default";
		$this->rendered = false;
		$this->printed = false;
		$this->content = "";
		$this->standAlone = false;
		$this->ts_inicio = microtime(true);
		$this->is_ajax = $this->isAjax();
		$this->template_dir = ABS_DIR."templates/";
		$this->is_module = false;
		
		$this->load_ajax_js =(isset($_REQUEST['output']) && $_POST['output'] == "json")?false:true;
		$this->put_additional_content =(isset($_POST['output']) && $_POST['output'] == "json")?false:true;
		
		$tmp = "";
		foreach($_GET as $key => $valor){
			if($key != "url")$tmp .= "$key=".urlencode($valor)."&";	
		}
		if($tmp != "")$tmp = "?".$tmp;
		$this->complete_url = $this->url.substr($tmp,0,-1);
	}
	function isAjax() {
		return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') || isset($_POST['ajx']));
	}
	function getInstance(){
		if(self::$instance == null){
			self::$instance = new self();
		}
		return self::$instance;
	}
	function get_execution_time($echo = true){
		$this->ts_fin = microtime(true);
		$execution_time = $this->ts_fin - $this->ts_inicio;
		if($echo){
			echo "\n<!-- Execution time: {$execution_time}s -->";
		}
		return $execution_time;
	}
	function get($key){
		$page = self::getInstance();
		return $page->$key;
	}
	function set($key,$valor){
		echo $key;
		$page = self::getInstance();

		$page->$key = $valor;
	}
	
	/* FUNCIONES GENERALES */
	function back(){
		echo '<script type="text/javascript">
			window.back();
		</script>';
	}
	//Javascript
	function importJS($str){
		$args = (is_array($str))?$str:func_get_args();
		foreach($args as $js){
			$js = "/js/$js.js";
			$this->javascripts[] = $js; 
		}
		
	}
	function addJS($js){$this->javascripts[] = $js; }
	
	//CSS
	function importCSS($str){
		$args = (is_array($str))?$str:func_get_args();
		foreach($args as $css){
			$css = "/css/$css.css";
			$this->css[] = $css; 
		}
		
	}
	function addCSS($css){$this->css[] = $css; }
		
	//Title
	function putTitle(){
		$title = $this->content->find("head title",0);
		if($title == NULL){
			$this->content->find("head",0)->innertext .= "\n\t<title>$this->title</title>"; 
		}else{
			$title->innertext = $this->title;	
		}
	}
	
	
	function putJS(){
		$head = $this->content->find("head",0);
		$salida = "\n\n\t<!-- Javascripts -->";
		foreach($this->javascripts as $js){
			$salida .= "\n\t<script type=\"text/javascript\" src=\"".Html::url($js)."\"></script>"; 	
		}
		$head->innertext .= $salida;
	}
	function putCSS(){
		$head = $this->content->find("head",0);
		$salida = "\n\n\t<!-- CSS -->";
		foreach($this->css as $css){
			$salida .= "\n\t<link rel=\"stylesheet\" type=\"text/css\" href=\"".Html::url($css)."\" />"; 	
		}
		$salida .= "\n\t<!--CSS THEME --><link rel=\"stylesheet\" type=\"text/css\" href=\"".Html::url($this->css_theme)."\" /><!--CSS THEME -->"; 
		$head->innertext .= $salida;
	}
	function putMetatags(){
		$head = $this->content->find("head",0);
		$salida = "\n\n\t<!-- Metatags -->";
		$salida .= "\n\t".'<meta http-equiv="Content-Type" content="text/html; charset='.$this->charset.'" />';
		$salida .= "\n\t<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"".$this->favicon."\" />";
		$head->innertext .= $salida;
	}
	
	function putHead(){
		$head = $this->content->find("head",0);
		$head->innertext .= $this->head;	
	}
	//Setters
	function setTemplate($str){
		if(!file_exists($this->template_dir.$this->template.".php"))die("El template especificado no existe");
		$this->template = $str;
	}
	function setCharset($str = "utf-8"){
		$this->charset = $str;
	}
	function setTheme($theme){
		$this->css_theme = "/".CSS_DIR."/$theme/estilos.css";
	}
	function setFavicon($str = "/favicon.ico"){
		$this->favicon = Html::url($str);
	}
	
	function bodyAppend($str){
		if(!$this->standAlone){
			$this->body = $str.$this->body;
		}
	}
	function bodyPrepend($str){
		if(!$this->standAlone){
			$this->body .= $str;
		}
	}
	function headAppend($str){
		if(!$this->standAlone){
			$this->body = $str.$this->body;
		}
	}
	function headPrepend($str){
		if(!$this->standAlone){
			$this->body .= $str;
		}
	}
	
	function load(){
		ob_start();
		$this->rendered = true;
		if($this->template == false){
			echo ($this->body);
		}else{
			if(!$this->is_ajax){
				foreach($GLOBALS as $key => $valor)global $$key;
				require($this->template_dir."/".$this->template.".php");
			}else{
				echo ($this->body);
			}
		}
		$this->content .= ob_get_contents();
		ob_clean();
		ob_end_flush();
	}
	function view(){
		if(!$this->rendered)$this->load();
		$this->printed = true;
		echo $this->content;
	}
	
} 
?>