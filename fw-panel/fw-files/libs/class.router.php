<?php
class Router{
	var $redirecciones = array();
	var $extensiones = array(".php",".html",".htm");
	
	function redirectFile($url,$destino,$template = null){
		if($destino{0} == "/")$destino = substr($destino,1);
		$this->redirecciones[$url] = $destino;
	}
	function redirect($url,$destino,$template = null){
		$this->redirecciones[$url] = $destino;
	}
	function get_preg_redireccion($url){
		$preg = false;
		foreach($this->redirecciones as $origen => $destino){
			$replac = "/^".str_replace("/","\/",$origen)."$/";
			if(preg_match($replac,$url)){
				$preg = true;
				$url = preg_replace("/".str_replace("/","\/",$origen)."/",$destino,$url);
				return $url;
			}
		}
		return false;
	}
	function get_redireccion($url){
		if($new_url = $this->get_preg_redireccion($url))$url = $new_url;
		else $url = $url;
		//Si no esta en el array de redirecciones...
		if($url == "")$url = "index";
		if(strrchr($url,"/")== "/")$url .= "index";
		if($url{0} == "/")$url = substr($url,1);
		if(!file_exists($url)){
			$tmp = $url."/";
			if($new_url = $this->get_preg_redireccion($tmp))echo $new_url;
		}
		return $url;
	}
	
	
	function go($url,$return = false){
		$params = $this->redirected_file["params"];
		$rfile = $this->redirected_file["file"];
		if($params != ""){
			$asignaciones = explode("&",$params);
			foreach($asignaciones as $asignacion){
				list($key,$valor) = explode("=",$asignacion);
				$_GET[$key] = ($valor);
			}
		}
		
		if($return)return $rfile;
		foreach($GLOBALS as $key => $valor)global $$key;
		if($controller !== false)call_user_func_array($controller,array());
		require($rfile);
	}
}?>