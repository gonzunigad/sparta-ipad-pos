<?php
class Conf{
	private static $instance;
	private static $configs;
	function Conf(){
		$this->configs = array();
	}
	function getInstance(){
		if(is_null(self::$instance)){
			self::$instance = new Conf();
		}
		return self::$instance;
	}
	public static function setAll($arr = false,$reemplazar = false){
		global $conf;
		if($arr === false)$arr = $conf;
		if($reemplazar){
			self::$configs = $arr;
		}else{
			foreach($arr as $key => $valor){
				self::$configs[$key] = $valor;
			}
		}
	}
	public static function set($codigo, $valor){
		self::$configs[$codigo] = $valor;
	}
	public static function read($codigo){
		if(!isset(self::$configs[$codigo]))return false;
		return self::$configs[$codigo];
	}
	
	public static function readAll(){
		$arr = array();
		foreach(self::$configs as $key => $valor)$arr[$key] = $valor;
		return $arr;
	}
}
?>