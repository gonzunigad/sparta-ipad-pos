<?php
class SiteAdmin{
	static $instance = null;
	var $con = false;
	var $filename = false;
	var $tmp_file = false;
	var $remotefile = false;
	var $info = false;
	
	function SiteAdmin(){
		$this->tmp_file = UPLOADS_DIR.rand(0,9999999);
	}
	function getInstance(){
		if(self::$instance == null){
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	
	function connect(){
		global $conf;
		$this->con = ftp_connect($conf["FTP.SERVER"]);
		$con = ftp_login($this->con,$conf["FTP.USER"],$conf["FTP.PWD"]);
		if(!$con)die(__("No se pudo conectar por FTP"));
	}
	function getFile($filename){
		$this->filename = $filename;
		global $conf;
		$file = pathinfo($filename);
		$remotefile = $conf["FTP.ABS"].ABS_URL.$file["basename"];
		$this->remotefile = $remotefile;
		ftp_get($this->con,$this->tmp_file,$remotefile,FTP_ASCII );	
	}
	function getFileContent($filename){
		$this->getFile($filename);
		return file_get_contents($this->tmp_file);
	}
	function getHtmlParser($filename){
		$tmp = $this->getFileContent($filename);
		$this->html = str_get_html($tmp);
	}
	
	function putNewContents(){
		if($this->filename != false){
			file_put_contents($this->tmp_file,$this->html);
			ftp_put ($this->con,$this->remotefile,$this->tmp_file,FTP_ASCII );
			unlink($this->tmp_file);
			ftp_close($this->con);
		}else{
			die("Tienes que bajar un archivo antes de volver a subirlo");	
		}
	}
	
}
?>