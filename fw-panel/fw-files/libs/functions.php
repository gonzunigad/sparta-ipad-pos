<?php
/* Format */
/* FUNCIONES UTILES */
function is_home(){
	global $page;
	return ( $page->complete_url == ABS_URL.'index.php' );
	
}
function add_listener($event,$function,$priority = 10){
    global $page;
    $page->event_listeners[$event][$priority][] = $function;
}
function add_new($event,$type,$function,$priority = 10){
	add_listener($event."_".$type,$function,$priority = 10);
}
function execute_listeners($event,$feedback = false,$returnFirst = false, $args = false){
    global $page;
	if(!is_array($args)){
		$args = array_slice(func_get_args(),3);
	}
	if($feedback)$result = $args[0];
    if(!isset($page->event_listeners[$event]) || !is_array($page->event_listeners[$event]))return false;
	ksort($page->event_listeners[$event]);
    foreach($page->event_listeners[$event] as $functions){
		foreach($functions as $function){
			$result = call_user_func_array($function,$args);
			if($returnFirst && $result !== false)return $result;
			if($feedback)$args[0] = $result;
		}
	}
	if($feedback)$result = true;
	return $result;	
}

function apply_parse($code,$value,$args = array()){
	$args = array_slice(func_get_args(),1);
	$res =execute_listeners($code,true,false,$args);
	return $res;
}
function trigger($event,$args=array()){
	$args = array_slice(func_get_args(),1);
	execute_listeners($event,false,false,$args);	
}	
function get_new($code,$type,$args = array()){
	$args = array_slice(func_get_args(),2);
	$res =execute_listeners($code."_".$type,false,true,$args);
	return $res;
}


function css_compress($buffer) {
	/* remove comments */
	$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
	/* remove tabs, spaces, newlines, etc. */
	$buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
	return $buffer;
}


/* MESAGES */
function addMessage($str,$type = "information"){
	$_SESSION["FW.MESSAGES"][] = array("msg" => $str,"type" => $type);
}
function printMessage($str,$type="information"){
	echo Component::get("msgbox",array("type" => $type,"msg" => $str));
}
function getMessages(){
	$str = "";
	if(isset($_SESSION["FW.MESSAGES"]) && is_array($_SESSION["FW.MESSAGES"])){
		foreach($_SESSION["FW.MESSAGES"] as $key => $valor){
			$str .= Component::get("msgbox",array("type" => $valor["type"],"msg" => $valor["msg"]));
		}
	}
	unset($_SESSION["FW.MESSAGES"]);
	return $str;
}
function Messages(){
	echo getMessages();
}
//TABLES
function edit_btn($id,$url = "edit",$additional_query= ""){
	global $page;
	return "<a href=\"".$url."?id=$id\" class=\"special option_edit\">".__("Editar")."</a>";	
}
function delete_btn($id,$url = "save",$additional_query= ""){
	global $page;
	return "<a href=\"".$url."?elim=$id\" class=\"special option_delete\">".__("Eliminar")."</a>";	
}
function state_btn($id,$state,$url="save",$additional_query = ""){
	global $page;
	$txt = (!$state)?__("Activar"):__("Desactivar");
	$new_state = ($state)?"0":"1";
	return "<a href=\"".$url."?id=$id&set_state=$new_state&$additional_query\" state=\"$new_state\" class=\"special option_state\">".$txt."</a>";
}
function row_options($row,$action,$estado,$editar,$eliminar){
	$txt = "<div class='reg_option'>";
	if($editar){
		$txt .= "<a href='$action' class='option_edit'>".__("Editar")."</a>";
	}
	if($eliminar){
		$txt .= "<a href='$action?elim=$reg_id' class='option_delete'>".__("Eliminar")."</a>";
	}
	if($estado){
		$txt .= "<a href='$action?' class='option_state'>".__("Estado")."</a>";
	}
	$txt .= "</div>";
	return $txt;
}
function acortar($str,$limit=100){
	if(strlen($str) > $limit-3)$str = substr($str,0,$limit)."...";
	return $str;
}
function vars2array($str,$checkOperador = true){
	$operadores = array("LIKE","<>","!=","<",">","=");
	if($str == "")return false;
	$tmp = explode("&",$str);
	$arr = array();
	foreach($tmp as $valor){
		explode(",",$valor);
		if($valor){
			foreach($operadores as $operador){
				if($pos = strpos($valor,$operador)){
					$key = substr($valor,0,$pos);
					$valor = substr($valor,$pos+strlen($operador));
					break;	
				}
			}
			if($checkOperador)$key = $key." ".$operador;
			$tmp = explode(",",$valor);
			if(count($tmp) > 1)$arr[$key] = $tmp;
			else $arr[$key] = $tmp[0];
		}
	}
	return $arr;
}
//CONFIGURACIONES
if(!isset($config))$config = false;
function get_configurations(){
	$config = array();
	$conf = Model::instance("Configuration");
	$conf->select("*","result");
	while($row = $conf->each()){
		$tmp[$row["conf_code"]] = $row["conf_value"];
	}
	return $tmp;
}
function get_config($code){
	global $config;
	if(!is_array($config)){
		$config = get_configurations();	
	}
	return (isset($config[$code]))?$config[$code]:false;
}
function set_config($code,$value){
	$conf = Model::instance("Configuration");
	return $conf->update(array("conf_value" => $value),array("conf_code" => $code));
}
function config($code){
	echo get_config($code);	
}
//if(!is_callable("json_encode")){
	include("json.php");
//}
function array2json($val){
	if(is_array($val)) return array2json($val);
	if(is_string($val)) return '"'.addslashes($val).'"';
	if(is_bool($val)) return 'Boolean('.(int) $val.')';
	if(is_null($val)) return '""';
	return $val;
}
//Plantillas
function plantilla($plantilla,$row,$pre=""){
	$res = $plantilla;
	if(is_array($row)){
		foreach($row as $key => $valor){
			if($pre != "")$key = "$pre.$key";
			$tmp_key = str_replace(".","\.",$key);
			$coincidencias = preg_match_all("/\[($tmp_key)([^[\]]*)\]/",$res,$matches);
			if(is_array($valor)){
				$res = plantilla($res,$valor,$key);
			}else{
				for($i = 0; $i < count($matches[0]);$i++){
					$nombre = $matches[1][$i];$modif = $matches[2][$i];$completo = $matches[0][$i];
					$formatos = explode(",",$modif);unset($formatos[0]);
					$val = reemplazos($valor,$formatos);
					$res = str_replace($completo,$val,$res);
				}
			}
		}
	}
	return $res;
}
function reemplazos($antes,$for){
	if(!is_array($for)){$formatos[] = $for;}else{$formatos = $for;}
	//SI NO TIENE MODIFICADORES, POR DEFECTO SE LE ASIGNA nl2br y HTMLENTITIES
	$res = (count($formatos) > 0)?$antes:nl2br($antes);
	foreach($formatos as $formato){
		$pc = substr($formato,0,1);
		if($pc == "#"){ // FORMATOS #
			switch(substr($formato,1)){
				case "NOHTML":
					$res = html_entity_decode(strip_tags($res));
					break;
				case "ISHTML":
					$res = $res;
					break;
				case "FECHA":
					$res = date("d-m-Y",strtotime($res));
					break;
				case "FECHAC":
					$res = FechaNormal($res);
					break;
				case "NUMERO":
					$res = _num($res);
					break;
				case "DINERO":
					$res = "$"._num($res);
					break;
				case "UPLOAD_FILE":
					$res = UPLOADS_URL.$res;
					break;
				case "MEDIA":
					switch(_tipo_medio($res)){
						case "imagen":
							$res = _rimgn($res);
							break;
						case "flash":
							$valores = getimagesize(UPLOADS_URL.$res);
							$res =  _rflash(UPLOADS_URL.$res,$valores[3]);
							break;
						default:
							$res =  "";
							break;
					}
					break;
			}
			if(strpos($formato,"FECHA:") !== false){
				$form = substr($formato,7);
				$res = FechaNormal($res,$form);
			}
			if(strpos($formato,"MEDIA:") !== false){
				$size = substr($formato,7);
				switch(_tipo_medio($res)){
					case "imagen":
						$res = _rimg($res,$size);
						break;
					case "flash":
						$res =  _rflash(UPLOADS_URL."$res",$size);
						break;
					default:
						$res =  "";
						break;
				}
			}
		}elseif($pc == "%"){ //CORTAR UN STRING
			$res = Acortar($res,substr($formato,1));
		}
	}
	return $res;
}
//HTML DOM PARSER FUNCTIONS
function menu_link($link){
	echo PANEL_URL.$link;
}
//MISC
function getMetaDescription($text) {
    $text = strip_tags($text);
    $text = trim($text);
    $text = substr($text, 0, 247);
    return $text."...";
}
function getMetaKeywords($text,$num=10) {
    // Limpiamos el texto
    $text = strip_tags($text);
    $text = strtolower($text);
    $text = trim($text);
    $text = preg_replace('/[^a-zA-Z0-9 -]/', ' ', $text);
    // extraemos las palabras
    $match = explode(" ", $text);
    // contamos las palabras
    $count = array();
    if (is_array($match)) {
        foreach ($match as $key => $val) {
            if (strlen($val)> 3) {
                if (isset($count[$val])) {
                    $count[$val]++;
                } else {
                    $count[$val] = 1;
                }
            }
        }
    }
    // Ordenamos los totales
    arsort($count);
    $count = array_slice($count, 0, $num);
    return implode(", ", array_keys($count));
}
/* Funciones para solucionar problemas con acentos */
//
//    utf8 encoding validation developed based on Wikipedia entry at:
//    http://en.wikipedia.org/wiki/UTF-8
//
//    Implemented as a recursive descent parser based on a simple state machine
//    copyright 2005 Maarten Meijer
//
//    This cries out for a C-implementation to be included in PHP core
//
    function valid_1byte($char) {
        if(!is_int($char)) return false;
        return ($char & 0x80) == 0x00;
    }
   
    function valid_2byte($char) {
        if(!is_int($char)) return false;
        return ($char & 0xE0) == 0xC0;
    }
    function valid_3byte($char) {
        if(!is_int($char)) return false;
        return ($char & 0xF0) == 0xE0;
    }
    function valid_4byte($char) {
        if(!is_int($char)) return false;
        return ($char & 0xF8) == 0xF0;
    }
   
    function valid_nextbyte($char) {
        if(!is_int($char)) return false;
        return ($char & 0xC0) == 0x80;
    }
   
    function valid_utf8($string) {
        $len = strlen($string);
        $i = 0;   
        while( $i < $len ) {
            $char = ord(substr($string, $i++, 1));
            if(valid_1byte($char)) {    // continue
                continue;
            } else if(valid_2byte($char)) { // check 1 byte
                if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                    return false;
            } else if(valid_3byte($char)) { // check 2 bytes
                if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                    return false;
                if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                    return false;
            } else if(valid_4byte($char)) { // check 3 bytes
                if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                    return false;
                if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                    return false;
                if(!valid_nextbyte(ord(substr($string, $i++, 1))))
                    return false;
            } // goto next char
        }
        return true; // done
    }
	
	function showString( $string ){
		return ( !valid_utf8($string) ) ? iconv("ISO-8859-1", "UTF-8", $string) : $string ;
	}
?>