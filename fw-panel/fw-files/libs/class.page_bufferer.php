<?php
class PB{
	static $head = false;
	static $body = false;
	static $buffering = false;
	static $template = "default";
	function start(){
		self::$buffering = true;
		ob_start();
	}
	function endhead(){
		//self::$head = ob_get_contents();
		Page::set("head",ob_get_contents());
		ob_clean();
	}
	function end(){
        global $page;
		//self::$body = ob_get_contents();
        Page::set("body",ob_get_contents());
        
		ob_clean();
		ob_end_flush();
	}

    function body_set_callback($valor){
        global $page;
        $page->body .= $valor;
    }
}

function microtime_float ()
{
    list ($msec, $sec) = explode(' ', microtime());
    $microtime = (float)$msec + (float)$sec;
    return $microtime;
}

?>