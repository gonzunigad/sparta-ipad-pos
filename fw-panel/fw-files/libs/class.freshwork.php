<?php
class Freshwork{
	static $instance = null;
	var $modules = array();
	var $models = array();
	var $controllers = array();
	var $base_jss = array();
	var $reload_jss = array();
	var $base_csss = array();
	
	
	function Freshwork(){
		$this->getInstalledModules();
		$this->getModels();
		$this->getControllers();
		$this->base_js_file = "panel_base_js.js";
		$this->reload_js_file = "panel_reload_js.js";
		$this->base_css_file = "panel.css";
		
		$this->addBaseJs(PANEL_DIR."resources/js/jquery-1.4.1.min.js");
	}
	function getInstalledModules(){
        global $jump_folders;
		$tmp = array();
		
		//Obtiene nombre de carpetas dentro de MODULES_DIR
		if ($dh = opendir(MODULES_DIR)) { 
			while (($module = readdir($dh)) !== false) { 
				if (is_dir(MODULES_DIR.$module) && strpos($module, ".") !== 0 && strpos($module, "_") !== 0 && !in_array($module,$jump_folders))
					$tmp[] = $module;
			}
		}
		closedir($dh);
		
		$this->modules = $tmp;
	}
	function getModels(){
		$tmp = array();
		/* ESTO DEBIESE DESAPARECER */
		if(file_exists(MODELS_DIR)){
			if ($dh = opendir(MODELS_DIR)) { 
				while (($model = readdir($dh)) !== false) { 
					if (!is_dir(MODELS_DIR.$model) && $model!="." && $model!="..")
						$tmp[MODELS_DIR.$model] = Model::file2name($model);
				}
			}
			closedir($dh);	
		}
		foreach($this->modules as $module){
			$dir = MODULES_DIR.$module."/models/";			
			/* ESTO SE DEBIESE QUEDAR SOLO */
			if(file_exists($dir)){
				if ($dh = opendir($dir)) { 
					while (($model = readdir($dh)) !== false) { 
						if (!is_dir($dir.$model) && $model!="." && $model!=".."){
							if(substr($model,-4) == ".php"){
								//Convertir nombre de Archivos a Camel Case with _
								$tmp[$dir.$model] = Model::file2name($model);
							}
						}
					}
				}
				closedir($dh);	
			}
		}	
		$this->models = $tmp;
	}
	function getControllers(){
		$tmp = array();
		/* ESTO DEBIESE DESAPARECER */
		if(file_exists(CONTROLLERS_DIR)){
			if ($dh = opendir(CONTROLLERS_DIR)) { 
				while (($model = readdir($dh)) !== false) { 
					if (!is_dir(CONTROLLERS_DIR.$model) && $model!="." && $model!="..")
						$tmp[CONTROLLERS_DIR.$model] = substr($model,0,-4);
				}
			}
			closedir($dh);	
		}
		foreach($this->modules as $module){
			$dir = MODULES_DIR.$module."/controllers/";			
			/* ESTO SE DEBIESE QUEDAR SOLO */
			if(file_exists($dir)){
				if ($dh = opendir($dir)) { 
					while (($model = readdir($dh)) !== false) { 
						if (!is_dir($dir.$model) && $model!="." && $model!=".."){
							if(substr($model,-4) == ".php"){
								//Convertir nombre de Archivos a Camel Case with _
								$words = explode("_",substr($model,0,-4));
								foreach($words as $key => $word)$words[$key] = ucfirst($word);
								$name = implode("_",$words);
								$tmp[$dir.$model] = $name;
							}
						}
					}
				}
				closedir($dh);	
			}
		}	
		$this->controllers = $tmp;
	}
	
	//BASE JAVASCRIPTS
	function addBaseJs($str,$prepend = false){
		$args = (is_array($str))?$str:func_get_args();
		foreach($args as $js){
			if(is_string($js)){
				if($prepend)array_unshift($this->base_jss, $js);
				else $this->base_jss[] = $js; 
			}
		}
	}
	
	function loadBaseJs(){
		$rewrite = false;
		if(file_exists(TMP_DIR.$this->base_js_file))$jsmtime = filemtime(TMP_DIR.$this->base_js_file);
		else $rewrite = true;
		
		foreach($this->base_jss as $js){
			if($rewrite)break;
			$mtime = filemtime($js);
			if($mtime >= $jsmtime)$rewrite = true;
			
		}
		//echo "<pre>";
		//print_r($this->base_jss);
		//echo "</pre>";
		if($rewrite){
			$str = "";
			foreach($this->base_jss as $js){
				$str .= file_get_contents($js);
			}
			file_put_contents(TMP_DIR.$this->base_js_file,JSMin::minify($str));
		}
		
	}
	function getBaseJsFilename($php = false){
		return 	($php)?TMP_DIR.$this->base_js_file:TMP_URL.$this->base_js_file."?v=".rand(1,221422);
	}
	function putBaseJs(){
		foreach($this->base_jss as $js){
			echo '<script type="text/javascript" src="'.substr($js,strlen(PANEL_DIR)).'"></script>'."\n\t";
		}
		/*echo '<script type="text/javascript" src="'.$this->getBaseJsFilename().'"></script>';*/
	}
	
	//RELOAD JAVSCRIPTS
	function addReloadJs($str){
		$args = (is_array($str))?$str:func_get_args();
		foreach($args as $js){
			$this->reload_jss[] = $js; 
		}
	}
	
	function loadReloadJs(){
		$rewrite = false;
		if(file_exists(TMP_DIR.$this->reload_js_file))$jsmtime = filemtime(TMP_DIR.$this->reload_js_file);
		else $rewrite = true;
		
		foreach($this->reload_jss as $js){
			if($rewrite)break;
			$mtime = filemtime($js);
			if($mtime >= $jsmtime)$rewrite = true;
			
		}
				
		if($rewrite){
			$str = "";
			foreach($this->reload_jss as $js){
				$str .= file_get_contents($js);
			}
			file_put_contents(TMP_DIR.$this->reload_js_file,JSMin::minify($str));
		}
		
	}
	function getReloadJsFilename($php = false){
		return 	($php)?TMP_DIR.$this->reload_js_file:TMP_URL.$this->reload_js_file;
	}
	function putReloadJs(){
		echo '<script type="text/javascript" src="'.$this->getReloadJsFilename().'"></script>';
	}
	
	//PANEL BASE CSS
	function addBaseCss($str){
		$args = (is_array($str))?$str:func_get_args();
		foreach($args as $css){
			$this->base_csss[] = $css; 
		}
	}
	
	function loadBaseCss(){
		$rewrite = false;
		if(file_exists(TMP_DIR.$this->base_css_file))$cssmtime = filemtime(TMP_DIR.$this->base_css_file);
		else $rewrite = true;
		
		foreach($this->base_csss as $css){
			if($rewrite)break;
			$mtime = filemtime($css);
			if($mtime >= $cssmtime)$rewrite = true;
			
		}
		
		if($rewrite){
			$str = "";
			foreach($this->base_csss as $css){
				$str .= file_get_contents($css);
			}
			file_put_contents(TMP_DIR.$this->base_css_file,css_compress($str));
		}
		
	}
	function getBaseCssFilename(){
		return 	TMP_URL.$this->base_css_file;
	}
	function putBaseCss(){
		foreach($this->base_csss as $css){
			$css = substr($css,strlen(PANEL_DIR));
			echo '<link type="text/css" media="screen" rel="stylesheet" href="'.$css.'" />'."\n\t";
		}
		//echo '<link type="text/css" media="screen" rel="stylesheet" href="'.$this->getBaseCssFilename().'" />';
	}
	
	static function getInstance(){
		if(self::$instance == null){
			self::$instance = new self();
		}
		return self::$instance;
	}
}
?>