<?php
if(isset($_REQUEST['ssid']) && $_REQUEST['ssid'] != ""){
	session_id($_REQUEST['ssid']);
}
session_start();
error_reporting(E_ALL ^ E_DEPRECATED);
ini_set("display_errors", 1);
/* OBTIENE RUTA ABSOLUTA COMO URL: EJ: '/blog/' ó simplemente '/' */
$p = substr(substr($_SERVER['SCRIPT_FILENAME'], 0, -32), strlen($_SERVER['DOCUMENT_ROOT'])) . "/";
if ($p{0} == "\\")$p = "/" . substr($p, 1);
if ($p{0} != "/")$p = "/$p";

/* DEFINICION DE CONSTANTES DEL SISTEMA XXXX_DIR */
define("ABS_DIR", dirname(dirname(dirname(dirname(__FILE__)))) . '/');
define("ABS_URL", $p);
define("PANEL_DIR", ABS_DIR . "fw-panel/");
define("FILES_DIR", PANEL_DIR . "fw-files/");
define("LIBS_DIR", FILES_DIR . "libs/");
define("INCS_DIR", FILES_DIR . "incs/");
define("RENDERS_DIR", FILES_DIR . "renders/");
define("TEMPLATE_DIR", PANEL_DIR . "templates/");
define("TEMPLATE_FORM_DIR", TEMPLATE_DIR . "form/");
define("COMPONENTS_DIR", PANEL_DIR . "components/");
define("CONTROLLERS_DIR", PANEL_DIR . "controllers/");
define("LOCALE_DIR", PANEL_DIR . "locale/");
define("MODELS_DIR", PANEL_DIR . "models/");
define("UPLOADS_DIR", FILES_DIR . "fw-uploads/");
define("STANDALONE_DIR", FILES_DIR . "standalone/");
define("MODULES_DIR", PANEL_DIR . "modules/");
define("TMP_DIR", FILES_DIR . "tmp/");
define("PANEL_RESOURCES_DIR", PANEL_DIR . "resources/");
define("PANEL_CSS_DIR", PANEL_RESOURCES_DIR . "css/");
define("PANEL_JS_DIR", PANEL_RESOURCES_DIR . "js/");


/* DEFINICION DE URLS DEL SISTEMA XXXX_URL */
define("PANEL_URL", ABS_URL . "fw-panel/");
define("RESOURCES_URL", PANEL_URL . "resources/");
define("FILES_URL", PANEL_URL . "fw-files/");
define("UPLOADS_URL", FILES_URL . "fw-uploads/");
define("LIBS_URL", FILES_URL . "libs/");
define("STANDALONE_URL", FILES_URL . "standalone/");
define("MODULES_URL", PANEL_URL . "modules/");
define("TMP_URL", FILES_URL . "tmp/");

/* DEFINICION DE RUTAS DE ARCHIVOS: XXXX_PATH */
define("CONFIG_PATH", FILES_DIR . "config.php");
define("ROUTER_PATH", FILES_DIR . "router.php");

define("ABS_LINK","http://".$_SERVER['SERVER_NAME'].ABS_URL);

//Configuraciones de sistema
$jump_folders = array(".", "..", "_notes");
$conf["INSTALL_SQL_FILE"] = "http://updates.freshworkstudio.com/install.sql";


/* SI NO EXISTE EL ARCHIVO, LANZAR CABECERA 404
  if(!file_exists(ABS_DIR.$_GET['url']) && !file_exists(CONTROLLERS_DIR.substr($_GET['url'],9))){
  header("HTTP/1.0 404 Not Found");
  exit;
  } */
?>