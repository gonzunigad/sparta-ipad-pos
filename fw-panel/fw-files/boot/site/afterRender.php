<?php
//$html = phpQuery::newDocument($page->content);
//include(RENDERS_DIR."fw_list.php"); // FW:LIST
//$page->content =$html;

//$page->content = $html->getString();
//print_r($page->body);


$html = str_get_html($page->content);
include(RENDERS_DIR."forms.php"); // FW:INPUT
include(RENDERS_DIR."fw_list.php"); // FW:LIST
//Setea el título
$title = $html->find("title");
if(isset($title[0])){
	$title[0]->innertext = sprintf(get_config("site_title"),$title[0]->innertext);
}
//INCLUYE JS PARA EDITAR EN LINEA
$head = $html->find("head");
if($head){
	//DEFINIR VARIABLES DEL FRESHWORK EN EL JS
	$head[0]->innertext .= "
<script type='text/javascript'>
	var ABS_URL 	= '".ABS_URL."'
	var PANEL_URL 	= '".PANEL_URL."'
	var MODULES_URL = '".MODULES_URL."'
	var UPLOADS_URL = '".UPLOADS_URL."'
	var STANDALONE_DIR = '".STANDALONE_DIR."'
</script>";
	if($page->check_session()){
		$comments = $html->find('comment');
		foreach($comments as $comment){
			$comment = substr($comment->innertext,4,-3);
			if(strpos($comment,"FWID:") !== false){
				list($key,$id) = split("FWID:",$comment);
				$page->pageCode = trim($id);
				break;
			}
		}
		
		$head[0]->innertext .= "
<script type='text/javascript'>
	var REQUEST_FILE	= '".$request_file."'
	var PAGEID			= '".$page->pageCode."'
</script>";
	}
	//INCLUIR LIBRERIAS DE EDICIÓN RÁPIDA SI ESTA CONECTADO AL PANEL
	if($page->check_session() && get_config("siteAdmin") !== "false")$head[0]->innertext .= 
	"\r\n\t".'<script type="text/javascript" src="'.PANEL_URL.'resources/js/shadowbox/shadowbox.js"></script>'.
	"\r\n\t".'<link href="'.PANEL_URL.'resources/js/shadowbox/shadowbox.css" rel="stylesheet" type="text/css" />'.
	"\r\n\t".'<script type="text/javascript" src="'.PANEL_URL.'resources/js/site/admin.js"></script>'.
	"\r\n\t".'<link href="'.PANEL_URL.'resources/css/site/admin.css" rel="stylesheet" type="text/css" />';
	
	//INCLUIR LIBRERIAS
	$a = get_config("libraries");
	$libs = explode(",",$a); $tmp = "";
	foreach($libs as $key => $valor){
		if($valor != "")$tmp .= "\r\n\t".'<script type="text/javascript" src="'.$valor.'"></script>';
	}
	$head[0]->innertext = $tmp.$head[0]->innertext;
}
$page->content = $html;
?>