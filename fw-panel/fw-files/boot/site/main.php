<?php
$url = $_GET['url'];
//Si existe la carpeta "templates", usar lógica de templates.
$page->template_dir = ABS_DIR."templates/";
if(!file_exists($page->template_dir)){
	$page->template = false;	
}
$functions_file = ABS_DIR."functions.php";
if(file_exists($functions_file))include_once "$functions_file";
/* LOGICA PARA ENCONTRAR EL ARCHIVO QUE SE ESTA SOLICITANDO EN LA URL */
$new_url = $router->get_redireccion($url);
$new_url_parse = parse_url($new_url);
$new_url_path = $new_url_parse["path"];
$ext = pathinfo($new_url_path);
$ext = (isset($ext["extension"]))?$ext["extension"]:"";
if(isset($new_url_parse["query"])){
	$get_plus = array();
	parse_str($new_url_parse["query"],$get_plus);
	$_GET = array_merge($_GET,$get_plus);
	$_REQUEST = array_merge($_REQUEST,$get_plus);
}
if(in_array($ext,$FW_VALID_EXTENSIONS)){
	$request_file = $new_url_path;
}else{
	foreach($FW_VALID_EXTENSIONS as $ext_valida){
		$request_file =  ABS_DIR.$new_url.".$ext_valida";
		if(file_exists($request_file))break;
	}
}
/* SI NO EXISTE EL ARCHIVO, LANZAR CABECERA 404  */
if(!file_exists($request_file)){
	header("HTTP/1.0 404 Not Found");exit;
}
PB::start();
$controller_file = dirname($request_file)."/cont.".basename($request_file); 
if(file_exists($controller_file))include($controller_file);
include($request_file);
PB::end();
//We include comments at the first lino and last line of the body
//$page->body = "<!--FW CONTENT BEGINS-->".$page->body."<!--FW CONTENT ENDS-->";
if($page->template != false)$page->body = "<div id=\"fw_begin_markup\">".$page->body."</div>";$page->load();include("afterRender.php");$page->view();?>