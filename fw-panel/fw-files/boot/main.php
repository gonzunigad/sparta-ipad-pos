<?php
include("setvars.php");


/* INCLUIR LIBRERIAS */
include(LIBS_DIR . "fw-config.php");
include(INCS_DIR . "extend.page.php");
include(LIBS_DIR . "class.db.php");
include(LIBS_DIR . "class.conf.php");
include(LIBS_DIR . "class.page_bufferer.php");
include(LIBS_DIR . "class.page.php");
include(LIBS_DIR . "class.module.php");
include(LIBS_DIR . "class.freshwork.php");
include(LIBS_DIR . "class.model.php");
include(LIBS_DIR . "class.controller.php");
include(LIBS_DIR . "class.component.php");
include(LIBS_DIR . "class.i18n.php");
include(LIBS_DIR . "class.form.php");

include(LIBS_DIR . "class.html_dom.php");
//include(LIBS_DIR . "class.phpQuery.php");

include(LIBS_DIR . "functions.php");
include(LIBS_DIR . "class.siteAdmin.php");
include(LIBS_DIR . "class.router.php");
include(LIBS_DIR . "class.excel.php");
include(LIBS_DIR . "class.jsmin.php");
include(INCS_DIR . "extend.model.php");

include(INCS_DIR."connect.db.php");

$page = Page::getInstance();
$fw = Freshwork::getInstance();
$i18n = i18n::getInstance();
$router = new Router();

/* CARGA PLUGINS/MODULES */
foreach ($fw->modules as $module) {
    include(MODULES_DIR . $module . "/main.php");
}

define("COMPLETE_URL", 'http://' . $_SERVER['SERVER_NAME'] . dirname($page->url) . '/');

include(ROUTER_PATH);

//Verify if magic_quotes directive is enabled
if(get_magic_quotes_gpc()){
  function undo_magic_quotes_array($array)  {
    return is_array($array) ? array_map('undo_magic_quotes_array', $array) : str_replace("\\'", "'",str_replace("\\\"", "\"",str_replace("\\\\", "\\",str_replace("\\\x00", "\x00", $array))));
  }
  $_GET = undo_magic_quotes_array($_GET);
  $_POST = undo_magic_quotes_array($_POST);
  $_COOKIE = undo_magic_quotes_array($_COOKIE);
  $_FILES = undo_magic_quotes_array($_FILES);
  $_REQUEST = undo_magic_quotes_array($_REQUEST);
}

function parsePostData($data) {
    if (isset($data) && is_array($data)) {
        foreach ($data as $key => $valor) {
            if (is_array($valor)) {
                $data["$key"] = parsePostData($valor);
            } else {
                $data["$key"] = ($valor);
            }
        }
    }
    return $data;
}
if (isset($_POST['data']))$data = parsePostData($_POST['data']);

/* SETEA UN TEMPLETE VACIO CUANDO SE TRATE DE UNA CONEXI�N AJAX */
if ($page->is_ajax)$page->template = false;


//Cambia ruta en la que se trabaja
chdir(ABS_DIR);


/* INLCUYE a panel/main.php si el arhivo que se est solicitando forma parte del panel de administración, site/main.php de lo contrario. */


if (preg_match("/^fw-panel/", $_GET['url'])) {
    include("panel/main.php");
} else {
    include("site/main.php");
}
if ($page->put_additional_content) {
    //echo "<!-- Execution time: ".$page->get_execution_time()." -->";
    $page->get_execution_time();
}
?>