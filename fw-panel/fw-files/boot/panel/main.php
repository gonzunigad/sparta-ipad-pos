<?php
$page->template_dir = TEMPLATE_DIR;

setcookie("fw_msgbox_tpl",Component::get("msgbox_js"));

$url = substr($_GET['url'],strlen("fw-panel")+1);

include("boot.php");

//SELECCION DE ARCHIVO EN BASE A LA URL SOLICITADA
$carpetas_de_sistema = array("components","controllers","fw-files","locale","models","modules","resources","templates");

$ext = pathinfo($url);
$ext = (isset($ext["extension"]))?$ext["extension"]:"";
if(in_array($ext,$FW_VALID_EXTENSIONS)){
	$request_file = PANEL_DIR.$url;
}else{
	foreach($FW_VALID_EXTENSIONS as $ext_valida){
		$request_file =  PANEL_DIR.$url.".$ext_valida";
		if(file_exists($request_file))break;
	}
}
//Obtiene el nombre de la primera carpeta después de fw-panel
//Ejemplo: fw-panel/products/list obtiene "products".
$fd = substr($url,0,strpos($url,"/")); 

//Si $fd no es el nombre de alguna carpeta del sistema, entonces se estará trabajando dentro de un módulo.
//Por lo tanto, fw-panel/products/list es una ruta corta para fw-panel/modules/products/list
if(!in_array($fd,$carpetas_de_sistema) && $fd != ""){	
	if(in_array($ext,$FW_VALID_EXTENSIONS)){
		$request_file = MODULES_DIR.$url;
	}else{
		foreach($FW_VALID_EXTENSIONS as $ext_valida){
			$request_file =  MODULES_DIR.$url.".$ext_valida";
			if(file_exists($request_file))break;
		}
	}
	$page->is_module = true;
	$page->module = $fd;
	$page->module_path = MODULES_DIR.$fd."/";
	$page->module_url = dirname($url)."/";
}

if($page->is_module){
	if(file_exists($page->module_path."config/config.php"))include($page->module_path."config/config.php");
	if(file_exists($page->module_path."config/functions.php"))include($page->module_path."config/functions.php");
	if(file_exists($page->module_path."controller.php"))include($page->module_path."controller.php");
}

/*$ruta_solicitada = substr($request_file,strpos($request_file,"fw-panel/")+9);
if($ruta_solicitada != "index.php" && !$page->is_ajax){
	//header("Location: ".PANEL_URL."#!$url");	
}*/
PB::start();
$controller_file = dirname($request_file)."/cont.".basename($request_file); 
if(file_exists($controller_file))include($controller_file);
if(file_exists($request_file))include($request_file);
PB::end();

if($page->is_ajax && $page->load_ajax_js && $page->put_additional_content){
	$page->body = '<script type="text/javascript" src="'.$fw->getReloadJsFilename().'"></script>'.$page->body;
	
}
if($page->put_additional_content)$page->body = Messages().$page->body; 

$page->load();
include("afterRender.php");

$fw->loadReloadJs();
$fw->loadBaseJs();
$fw->loadBaseCss();
$page->view();
?>