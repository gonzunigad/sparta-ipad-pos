<?php
global $fw;
//fw:title
$titles = $html->find("fw:title");
if($titles){
	foreach($titles as $title){
		$title->tag = "h2";
	}
}
//fw:subtitle
$subtitles = $html->find("fw:subtitle");
if($subtitles){
	foreach($subtitles as $title){
		$title->tag = "p";
		$title->id="page-intro";
	}
}

$head = $html->find("head");
if($head){
	//DEFINIR VARIABLES DEL FRESHWORK EN EL JS
	$head[0]->innertext = "
<script type='text/javascript'>
	var ABS_URL 	= '".ABS_URL."'
	var PANEL_URL 	= '".PANEL_URL."'
	var MODULES_URL = '".MODULES_URL."'
	var UPLOADS_URL = '".UPLOADS_URL."'
	var STANDALONE_DIR = '".STANDALONE_DIR."'
</script>".$head[0]->innertext;



}
/* CONTROLS */
//fw:delete
?>