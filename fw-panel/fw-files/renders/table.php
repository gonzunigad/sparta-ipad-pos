<?php
$tables = $html->find("fw:table");
foreach($tables as $table){
	$table->tag = "table";
	$new_html = '<thead><tr>';
	$columnas = explode(",",$table->fields);
	foreach($columnas as $columna){
		if($columna == "[select]")$cont = '<input class="check-all" type="checkbox" />';
		else $cont = $columna;
		$new_html .= "<th>".$cont."</th>";
	}
	$rows = $table->find("tr");
	foreach($rows as $row){
		$row->tag = "tr";
		$reg_id = $row->reg;
		$options = $row->find("fw:options");
		foreach($options as $option){
			$txt = "<div class='reg_option'>";
			if($option->edit){
				$txt .= "<a href='".$option->action."' class='option_edit'>".__("Editar")."</a>";
			}
			if($option->delete){
				$txt .= "<a href='".$option->action."?elim=$reg_id' class='option_delete'>".__("Eliminar")."</a>";
			}
			if($option->state){
				$txt .= "<a href='".$option->action."?' class='option_state'>".__("Estado")."</a>";
			}
			$txt .= "</div>";
			$option->outertext = $txt;
		}
	}
	$new_html .= "</tr></tr></thead>";
	$new_html .= "<tbody>";
	$new_html .= $table->innertext;
	$new_html .= "</tbody>";
	
	$table->innertext=$new_html;
}
?>