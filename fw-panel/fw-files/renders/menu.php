<?php
$sb = $html->find("fw:sidebar",0);
if($sb){
	$menus = $sb->find("fw:menu");
	foreach($menus as $menu){
		if($menu->code != "")$menu->code = $menu->code."/";
		$tmp = "";
		$new_html = "";
		$submenus = $menu->find("fw:submenu");
		foreach($submenus as $submenu){
			$tmp .= "\n\t".'<li><a href="'.PANEL_URL."#!".$menu->code.$submenu->href.'">'.__($submenu->innertext).'</a></li>';
		}
		if($tmp == ""){
			$new_html .= "\n".'<li><a href="'.PANEL_URL."#!".$menu->code.$menu->href.'" class="nav-top-item no-submenu">'.__($menu->name).'</a></li>';
		}else{
			$new_html .= "\n".'<li><a class="nav-top-item">'.__($menu->name).'</a><ul>'.$tmp.'</ul></li>';
		}
		$menu->outertext = $new_html;	
	}
	$sb->tag = "ul";
	$sb->id = "main-nav";
}
?>