<?php
$lists = $html->find("fw:list");
foreach($lists as $list){
	$list->class="from_panel";
	$model = Model::instance($list->mod);
	$model->cond = vars2array($list->cond);
	$model->limit = $list->limit;
	$model->order = ($list->order != "RANDOM")?vars2array($list->order,false):"RANDOM";
	$list->outertext = $model->get_listar($list->innertext);
}
?>