<?php
$containers = $html->find("fw:radios");
if($containers){
	foreach($containers as $container){
		$interior = "";
		$name = Form::dot2name($container->name);
		$container->tag = "div";
		$container->class .= "input radios";
		if(isset($container->label))$interior .= "<label>".$container->label."</label>";
		if(isset($container->description))$interior .= "<small>".$container->description."</small>";
		$radios = $container->find("fw:radio");
		$i = 1;
		foreach($radios as $radio){
			$tmp = "";
			$radio->tag = "div";
			$radio->class .= " radio";
			$selected = "";
			if(isset($GLOBALS["data"][$container->name])){
				if($GLOBALS["data"][$container->name] == $radio->value){
					$radio->class .= " selected"; 
					$selected = "checked=\"checked\"";
				}
			}
			$tmp = "<label for=\"fw_{$name}_{$i}\">".$radio->label."</label>";
			$tmp .= "<input $selected type=\"radio\" id=\"fw_{$name}_{$i}\" value=\"".$radio->value."\" name=\"$name\" />";
			if(isset($radio->description)) $tmp .= "<span>".$radio->description."</span>";
			$radio->label = false;$radio->description = false;$radio->value = false;
			$radio->innertext = $tmp;
			$i++;
		}
		
		if($interior != ""){
			$interior = "<div class=\"radios_wrapper\">$interior</div>";	
		}
		$container->innertext = $interior.$container->innertext;
	}
}

?>