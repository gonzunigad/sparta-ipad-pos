<?php
//fw:input - file
$files = 1;
$inputs = $html->find("fw:input");
if($inputs){
	foreach($inputs as $input){
		//Validaciones 
		if(!isset($input->name))$input->outertext = __("No ingresó el atributo 'name'");
		if(!isset($input->class))$input->class = "";
		
		//Obtiene el valor y el nombre que tendrá el input
		$name = Form::dot2name($input->name);
		$valor = Form::valueFromName($input->name);
		$inputTxt = "";
		if($input->type == "file"){ //FW:INPUT TYPE="FILE"
			$id = (isset($input->id))?$input->id:"file$files";
			$inputTxt = '
			<div class="input file">';
				$label = ($input->label != "")?$input->label:__("Modificar archivo");
				$inputTxt .= '
				<label for="'.$id.'">'.$label.'</label>';
			if($valor != "")$inputTxt .= '
				<span class="current"><strong>Archivo actual: </strong>
				'.$valor.'| <a class="noajax" target="_blank" href="'.UPLOADS_URL.$valor.'">Ver</a></span><a href="#;" class="delete">X</a>';
			$inputTxt .= '
				<input id="'.$id.'" type="file" name="'.$name.'" />
				<input type="hidden" id="'.$id.'Value" name="'.$name.'" value="'.$valor.'" />
			</div>';
			$files++;
		}elseif($input->type == "image"){ //FW:INPUT TYPE="IMAGE"
			$id = (isset($input->id))?$input->id:"file$files";
			$label = 
			$inputTxt = '
			<div class="input file image">
				<div class="img">';
				if($valor != "")$inputTxt .= '<img src="'.UPLOADS_URL.$valor.'" width="150" />';
				$label = ($input->label != "")?$input->label:__("Modificar imagen");
				$inputTxt .= '
				</div>
				<div class="info">
					<label for="'.$id.'">'.$label.'</label>';
					if($valor != "")$inputTxt .= '
					<span class="current"><strong>'.__("Imagen actual").': </strong><span class="filename">'.$valor.'</span></span>';
					$inputTxt .= '
					<div class="upload">
						<input id="'.$id.'" type="file" name="'.$name.'" />
						<input type="hidden" id="'.$id.'Value" name="'.$name.'" value="'.$valor.'" />
					</div>
					';
					if($valor != "")$inputTxt .= '<a href="#;" alt='._("Eliminar foto").' class="delete">X</a>';
					else $inputTxt .= '<a href="#;" alt='.__("Eliminar foto").' style="display:none" class="delete">X</a>';
					if($valor != "")$inputTxt .= '<a href="#;" class="reset">'.__("Deshacer cambios").'</a>';
					$inputTxt .= '
				</div>
				
			</div>';
			$files++;
		}elseif($input->type=="multiselect"){
			$include = (isset($input->include))?"include='".$input->include."'":"";
			$inputTxt = '
			<div class="input container multiselect text '.$input->class.'">
				<div class="wrapper">';
				if($input->label != ""){
					$inputTxt .= '<label>'.$input->label.'</label>'; 
				}
				if(isset($input->error) && $input->error != ""){
					$inputTxt .= '<span class="error">'.$input->error.'</span>';
				}
				if(isset($input->desc) && $input->desc != ""){ 
					$inputTxt .= '<small>'.$input->desc.'</small>';
				}
				$inputTxt .='<div class="new">
					<input type="text" class="multiselect_text" '.$include.' url="'.$input->url.'" />
					<input type="hidden" class="multiselect_hidden" />			
					</div>
					<div class="multiselect_options" name="'.$name.'"></div>';
				$inputTxt .= '
				</div>
			</div>';
		}elseif($input->type=="text"){ //FW:INPUT TYPE="TEXT"
			$inputTxt = Form::text($input->name,array("echo" => false,"label" => $input->label,"desc" => $input->desc,"class" => $input->class));
		}elseif($input->type=="select"){ //FW:INPUT TYPE="SELECT"
			$options = array();
			$opts = $input->find("fw:option");
			foreach($opts as $opt)$options[$opt->value] = $opt->innertext;
			$inputTxt = Form::select($input->name,array("options" => $options,"echo" => false,"label" => $input->label,"desc" => $input->desc,"class" => $input->class));
		}elseif($input->type=="date"){ //FW:INPUT TYPE="DATE"
			$inputTxt = Form::date($input->name,array("echo" => false,"label" => $input->label,"desc" => $input->desc,"class" => $input->class));
		}elseif($input->type=="password"){ //FW:INPUT TYPE="PASSWORD"
			$inputTxt = Form::password($input->name,array("echo" => false,"label" => $input->label,"desc" => $input->desc,"class" => $input->class));
		}elseif($input->type=="textarea"){ //FW:INPUT TYPE="TEXTAREA"
			$inputTxt = Form::textarea($input->name,array("echo" => false,"label" => $input->label,"desc" => $input->desc,"class" => $input->class));
		}elseif($input->type=="submit"){ //FW:INPUT TYPE="SUBMIT"
			$inputTxt = Form::submit($input->value,array("echo" => false));
		}elseif($input->type=="hidden"){ //FW:INPUT TYPE="HIDDEN"
			if(isset($input->value))$valor = $input->value;
			$inputTxt = Form::hidden($input->name,array("value" => $valor,"echo" => false));
		}else{
			$inputTxt = get_new("form_type",$input->type,$name, $valor, $input);
		}
		$input->outertext = $inputTxt;
	}
}


//fw:input-container
$containers = $html->find("fw:input-container");
if($inputs){
	foreach($containers as $container){ $valor = '
		<div class="input container '.$container->class.'">
			<div class="wrapper">';
		if($container->label != ""){
			$valor .= '<label>'.$container->label.'</label>'; 
		}
		if(isset($container->error) && $container->error != ""){
			$valor .= '<span class="error">'.$container->error.'</span>';
		}
		$valor .= $container->innertext;
		if(isset($container->desc) && $container->desc != ""){ 
			$valor .= '<small>'.$container->desc.'</small>';
		}
		$valor .= "</div></div>";
		$container->outertext = $valor;
	}
	
}
//FORM 
$forms = $html->find("fw:form");
if($forms){
	foreach($forms as $form){
		$form->tag="form";
		$action = $form->action;
		if(isset($form->type)){
			if(strpos($action,"?") !== -1)$action .= "&type=".$form->type;
			else $action .= "?type=".$form->type;
			
			unset($form->type);
		}
		$form->action = $page->module_url.$action;
	}	
}
?>