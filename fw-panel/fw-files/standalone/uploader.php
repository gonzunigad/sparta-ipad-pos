<?php
include("../boot/setvars.php");
/*
Uploadify v2.1.0
Release Date: August 24, 2009

Copyright (c) 2009 Ronnie Garcia, Travis Nickels

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

if (!empty($_FILES)) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = (dirname(dirname(__FILE__)))."/fw-uploads/";
	$name = preg_replace("/^[^a-z0-9]?(.*?)[^a-z0-9]?$/i", "$1", $_FILES['Filedata']['name']); //Clean the string from special characters.
	$newname = rand(100000,999999).".".date("Ymdhis").".".substr(str_replace(" ","_",$name),-30,30); //Create a new random name for the file
	$targetFile =  str_replace('//','/',$targetPath) . $newname;
	
	// $fileTypes  = str_replace('*.','',$_REQUEST['fileext']);
	// $fileTypes  = str_replace(';','|',$fileTypes);
	// $typesArray = split('\|',$fileTypes);
	// $fileParts  = pathinfo($_FILES['Filedata']['name']);
	
	// if (in_array($fileParts['extension'],$typesArray)) {
		// Uncomment the following line if you want to make the directory if it doesn't exist
		// mkdir(str_replace('//','/',$targetPath), 0755, true);
		
		if(move_uploaded_file($tempFile,$targetFile)){
			die("{estado:true,filename:'$newname',url:'".UPLOADS_URL."$newname'}");
		}else{
			die("{estado:false,msg:'".__("Error trying to upload the file.")."'}");
		}
		
	// } else {
	// 	echo 'Invalid file type.';
	// }
}
die("{estado:false}");
?>