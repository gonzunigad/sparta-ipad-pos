<?php 
$isEdit = (isset($_GET['id']))?true:false; 
$verb = ($isEdit)?$mod_verb[0]:$mod_verb[1];
$action = ($isEdit)?"save?type=edit&id=$_GET[id]":"save?type=add";
if($isEdit){
	$mod= Model::instance($mod_model);
	$data[$mod_model] = $mod->getById($_GET['id']);	
}

$mod= Model::instance($mod_model);
$mod->returnFormat = "array";
$res = $mod->getSimpleArray("nombre");
?>
<fw:title><?php ___("$verb ".strtolower($mod_object)); ?></fw:title>

<fw:container title="Formulario">
	<div class="form">
		<form action="<?php echo $action; ?>" method="post">
		<fw:input type="text" label="Nombre: " name="<?php echo $mod_model; ?>.nombre"></fw:input>
		<fw:input type="text" label="Jefe de tienda: " name="<?php echo $mod_model; ?>.jefe_tienda"></fw:input>
		<fw:input type="text" label="Teléfonos: " name="<?php echo $mod_model; ?>.telefono"></fw:input>
		<fw:input type="password" label="Contraseña: " name="<?php echo $mod_model; ?>.password"></fw:input>
		<!--<fw:input type="text" label="Dirección: " name="<?php echo $mod_model; ?>.direccion"></fw:input>-->
		<fw:input type="textarea" label="Mail de confirmación: " name="<?php echo $mod_model; ?>.mails_confirmacion"></fw:input>
		<fw:input type="textarea" label="Mail de cotización: " name="<?php echo $mod_model; ?>.mails_cotizacion"></fw:input>

		<div class="input submit">
			<input type="submit" class="button" value="<?php ___("$verb ".strtolower($mod_object)); ?>" />
		</div>
		</form>
	</div>
</fw:container>