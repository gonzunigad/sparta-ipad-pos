<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$mod_model = "Categoria";
$mod_object = "Categoría";
$mod_object_plural = "Categorías";
$mod_verb = array(__("Editar"),__("Crear nueva"));

$mod_msg_edit = "$mod_object editada satisfactoriamente";
$mod_msg_new = "$mod_object creada satisfactoriamente";
?>