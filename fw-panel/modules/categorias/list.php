<?php
$products = Model::instance($mod_model);
$products->paginate = false;
$products->results_per_page = 20;
$products->returnFormat = "array";
$arr = $products->select();
$paginador = $products->paginador(5);	

$arr2 = $arr;
$arr3 = $arr;
?>

<style type="text/css">
.sub-hijos{display:none; }
.sub-hijos ul,.sub-hijos li{list-style:none; background:none !important; padding:0 !important; }
.sub-hijos ul{position:relative; width:50%; float:left; margin-top:20px; }
.sub-hijos li{position:relative; width:120px; float:left; text-align:center; margin-right:10px; }
.sub-hijos li .nombre{display:none; position:absolute; padding:4px; background:-webkit-gradient(linear,0% 0%, 0% 100%, from(#fff),to(#ccc)); border:solid 1px #999; border-radius:3px; top:40%; left:0; width:90%; }
.sub-hijos li .nombre span{font-weight:bold; display:block; margin-bottom:10px; }


.sub-hijos li:hover .nombre,.sub-hijos li:hover .reg_option{display:block; }

table tr:hover td{background:#ccc; cursor:pointer; }
table tr.current td{background:#aaa; }
</style>
<script type="text/javascript">
$(function(){
	$(".menu2").click(function(){
		var catid = $(this).attr("cat_id");
		$(".sub-hijos").hide();
		$("#sub-hijo-"+catid).fadeIn();
		$(this).addClass("current").siblings().removeClass("current");
	});
	
});
</script>

<?php foreach($arr as $key => $valor){
	$hijos = array();
	if($valor["Categoria"]["cat_padre"] == 0){ ?>
	<fw:container title="<?php echo $valor["Categoria"]["nombre"]; ?>">
		<div style="width:20%; float:left; ">
			<img style="width:70%; max-width:175px; margin:0 auto; display:block;" src="<?php echo UPLOADS_URL.$valor["Categoria"]["imagen"]; ?>" alt="imagen" />
			<?php 
			echo "<div class='reg_option' style='float:none; width:120px; margin:0 auto; display:block;'>
				<a href='registro?id=$valor[id]'>".__("Editar")."</a>
				".delete_btn($valor["id"],"save")."
				".state_btn($valor["id"],$valor["activo"],"save")."
			</div>"; ?>
		</div>
		
		<fw:table style="width:60%;" fields = "Imagen, Nombre, Opciones">
			<?php
			foreach($arr2 as $key2 => $valor2){
				if($valor2["cat_padre"] == $valor["Categoria"]["id"]){
					$hijos[] = $valor2["Categoria"]["id"];
					echo "
					<tr class='menu2' cat_id='$valor2[id]'>
						<td><img src='".UPLOADS_URL."$valor2[imagen]' width='100' /></td>
						<td style='vertical-align:middle'>$valor2[nombre]</td>
						<td style='vertical-align:middle'>
							<div class='reg_option'>
								<a href='registro?id=$valor2[id]'>".__("Editar")."</a>
								".delete_btn($valor2["id"],"save")."
								".state_btn($valor2["id"],$valor2["activo"],"save")."
							</div>
						</td>
					</tr>";
				}
			}
			?>
		</fw:table>
		<?php 
		foreach($hijos as $val){
			echo "<div class='sub-hijos' id='sub-hijo-$val'><ul>";
			foreach($arr3 as $key3 => $valor3){
				if($valor3["Categoria"]["cat_padre"] == $val){
					echo "
					<li>
						<img src='".UPLOADS_URL."$valor3[imagen]' width='100' />
						<div class='nombre'>
							<span>$valor3[nombre]</span>
						
							<div class='reg_option'>
								<a href='registro?id=$valor3[id]'>".__("Editar")."</a>
								".delete_btn($valor3["id"],"save")."
								".state_btn($valor3["id"],$valor3["activo"],"save")."
							</div>
						</div>
						
					</li>";
				}	
			}echo "</ul></div>";
		}?>
		<div style="clear:both;"></div>
	</fw:container>
<?php }
}?>

