<?php

if($page->is_ajax){ ?>	
<script>
	window.location = "<?php echo PANEL_URL."cotizacion/export"; ?>";
</script>
<?php 
exit;
}




$filename ="cotizaciones.".mktime().".xls";
header('Content-type: application/ms-excel');
header('Content-Disposition: attachment; filename='.$filename);


$page->template = false;

$products = Model::instance("Cotizacion");
$products->select();
?>

<fw:table fields = "Nombre, Direccion, Comuna, Ciudad, Email, Rut, Genero">
<?php
while($row = $products->each()){ 
	echo "
	<tr>
		<td>$row[nombre]</td>
		<td>$row[direccion]</td>
		<td>$row[comuna]</td>
		<td>$row[ciudad]</td>
		<td>$row[email]</td>
		<td>$row[rut]</td>
		<td>$row[genero]</td>
	</tr>";
}
?>
</fw:table>
