<?php
$products = Model::instance($mod_model);
$products->paginate = true;
$products->results_per_page = 20;
$products->select();
$paginador = $products->paginador(5);	
?>




<fw:container title="Listado de <?php echo strtolower($mod_object_plural); ?>">
	<fw:table fields = "Nombre, URL,   Opciones">
		<tfoot>
			<tr><td colspan="10"><?php echo $paginador; ?></td></tr>
		</tfoot>
		<?php
		while($row = $products->each()){ 
			echo "
			<tr>
				<td>$row[nombre]</td>
				<td>$row[url]</td>
				<td>
					<div class='reg_option'>
						<a href='registro?id=$row[id]'>".__("Editar")."</a>
						".delete_btn($row["id"],"save")."
					</div>
				</td>
			</tr>";
		}
		?>
	</fw:table>
</fw:container>

