<?php
$model = $mod_model;
$marca = Model::instance($model);

//ELIMINAR
if(isset($_REQUEST['elim'])){
	if($marca->delete($_REQUEST['elim'])){
		die("{estado:true}");
	}else{
		die("{estado:false}");
	}
}

//ACTIVAR - DESACTIVAR
if(isset($_GET['set_state'])){
	if($marca->update(array("active" => $_POST['set_state']),$_GET['id'])){
		$msg = ($_POST['set_state'])?"Desactivar":"Activar";
		die("{estado:true,btn_msg:'$msg'}");
	}else{
		die("{estado:false}");
	}
}


//EDITAR - CREAR
if(isset($data) && isset($_GET['type'])){
	$msj ="";
	/* REGLAS DE VLIDACION */
	if(trim($data[$model]["nombre"]) == "")$msj .= "Debe ingresar un nombre<br />";
	if($msj != ""){
		die("{error:1,msg:'<strong>Solucione los siguientes problemas:</strong><br />$msj'}");	
	}
	
	if($_GET['type'] == "add"){
		if($marca->insert()){
			die("{msg:'$mod_msg_new ¿Desea continuar creando?',type:'success',allowsubmit:true,clear:true}");
		}else{
			die("{error:1,msg:'Ocurrió un problema al ingresar los datos.'}");		
		}
	
		exit;
	}elseif($_GET['type'] == "edit"){
		if($marca->update(false,$_GET['id'])){
			die("{error:0,msg:'$mod_msg_edit',type:'success',allowsubmit:true}");
		}else{
			die("{error:1,msg:'Ocurrió un problema al editar los datos.'}");		
		}
		exit;	
	}
}

?>