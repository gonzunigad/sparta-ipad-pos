<script type="text/javascript">
$(function(){
	$("#fie_name").keyup(function(){
		var text = this.value.toLowerCase().replace(/ /g,"_");
		text = text.replace("á","a").replace("é","e").replace("í","i").replace("ó","o").replace("ú","u").replace("ñ","n")
		document.getElementById("fie_code").value = text;
	});
});
function reloadFields(resp){
	alert(resp);
	cargar(window.location.hash.substring(2));
}
</script>
<fw:title>Administrar campos del m&oacute;dulo '<?php echo $module["mod_name"]; ?>' (<?php echo $module["mod_code"]; ?>)</fw:title>

<fw:container title="Nuevo campo" style="width:40%; float:left;">
	<fw:form action="save_field?type=add" class="form">
		<fw:input type="hidden" name="Field.mod_id"></fw:input>
		
		<fw:input type="text" name="Field.fie_name" label="Nombre" desc="Nombre visible por el usuario"></fw:input>
		
		<fw:input type="text" name="Field.fie_code" label="C&oacute;digo"></fw:input>
		
		<?php Form::select("Field.fie_type",array("label" => "Tipo de campo","options"	=> $types)); ?>
		
		<fw:input type="submit" value="Agregar campos"></fw:input>
	</fw:form>
</fw:container>

<!--TABLE DE CAMPOS -->
<fw:container title="Listado de campos" style="width:58%; float:right;">
	<fw:table fields="Nombre,C&oacute;digo,Tipo,Opciones"  id="fields_list">
		<?php
		foreach($fields as $field){
			echo "
			<tr>
				<td><span onclick='alert(\"hola\")'>$field[fie_name]</a></td>
				<td>$field[fie_code]</td>
				<td>$field[fie_type]</td>
				<td>
					".delete_btn($field["fie_id"],"mods/save_field")."
					<a href='javascript:alert(\"chao\");'>Editar</a>
				</td>
			</tr>";
		}?>
	</fw:table>
</fw:container>