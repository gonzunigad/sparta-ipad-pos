<script type="text/javascript">
$(function(){
	$("#mod_name").keyup(function(){
		var text = this.value.toLowerCase().replace(/ /g,"_");
		text = text.replace("á","a").replace("é","e").replace("í","i").replace("ó","o").replace("ú","u").replace("ñ","n")
		document.getElementById("mod_code").value = text;
		document.getElementById("mod_table").value = "fw_"+text;
	});
});
</script>

<p>Al crear un nuevo m&oacute;dulo, se crear&aacute; una tabla en la base de datos.</p>
<form action="panel/mods/save?type=add" method="post" class="form">
	<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
		<fw:input type="text" name="Module.mod_name" label="Nombre" desc="Nombre descriptivo para el m&oacute;dulo"></fw:input>
		<fw:input type="text" name="Module.mod_code" label="C&oacute;digo"></fw:input>
		<fw:input type="text" name="Module.mod_table" label="Nombre de la tabla" desc="Nombre de la tabla que se generar&aacute; en el sistema. Si ya existe, se intentar&aacute; usar el modelo actual"></fw:input>
		<p>
			<input class="button" type="reset" value="<?php ___("Cancelar"); ?>" /> 
			<input class="button" type="submit" value="<?php ___("Crear m&oacute;dulo"); ?>" />
		</p>
	</fieldset>
	<div class="clear"></div><!-- End .clear -->
	
</form>
