<?php
$librerias = array(
	"jQuery 1.3.2" 			=> "http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js",
	"jQuery UI 1.7.2"		=> "http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js",
	"Prototype 1.6.0.3"		=> "http://ajax.googleapis.com/ajax/libs/prototype/1.6.0.3/prototype.js",
	"Script.acolo.us 1.8.2"	=> "http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.2/scriptaculous.js",
	"MooTools 1.2.3"		=> "http://ajax.googleapis.com/ajax/libs/mootools/1.2.3/mootools-yui-compressed.js",
	"Dojo 1.3.1"			=> "http://ajax.googleapis.com/ajax/libs/dojo/1.3.1/dojo/dojo.xd.js",
	"SwfObject 2.2"			=> "http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"
);
$conf = Model::instance("Configuration");
$conf->cond["conf_code"] = "'libraries'";
$select = $conf->getResult();
$seleccionados = explode(",",$select["conf_value"]);
?>