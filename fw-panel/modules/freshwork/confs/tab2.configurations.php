<?php
$conf = Model::instance("Configuration");
$select = $conf->select();
while($row = $conf->each()){
	$data[$row["conf_code"]] = $row["conf_value"];
}
?>

<form action="save_configurations?form=basic" onSend="return false;" method="post" class="form">

	<div class="config">
		<div class="col1">
			<strong><?php ___("Extensiones en la URL:"); ?> </strong><br />
			<small>Especif&iacute;ca si las URL en el sitio utilizar&aacute;n o no extensiones</small>
		</div>
		
		<fw:radios class="col2" name="site_extensions">
			<fw:radio value="both" label="Mixto" 
				description="Tanto 'www.example.com/index' como 'www.example.com/index.php' son v&aacute;lidas">
			</fw:radio>
			<fw:radio value="no" label="No mostrar" 
				description="Solo 'www.example.com/index' es una URL v&aacute;lida">
			</fw:radio>
			<fw:radio value="yes" label="Mostrar" 
				description="Solo 'www.example.com/index.php' es una URL v&aacute;lida">
			</fw:radio>
		</fw:radios>
		
		<div class="clear"></div>
	</div>
	
	<div class="config">
		<div class="col1">
			<strong><?php ___("Habilitar Site Admin"); ?> </strong><br />
			<small>Especifíca si se habilitará o no la edición en vivo del sitio web cuando estés logeado.</small>
		</div>
		
		<fw:radios class="col2" name="siteAdmin">
			<fw:radio value="false" label="Deshabilitar"></fw:radio>
			<fw:radio value="true" label="Habilitar"></fw:radio>
		</fw:radios>
		
		<div class="clear"></div>
	</div>
	
	<?php Form::submit("Guardar cambios"); ?>
</form>