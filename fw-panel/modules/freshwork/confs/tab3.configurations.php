<style type="text/css">
	.librerias div{padding:4px; overflow:hidden; }
	.librerias div label{float:left; width:140px; clear:left; cursor:pointer; }
	.librerias div input{float:left;}
	.librerias div:hover{background:lightyellow; }
</style>

<form action="save_configurations?form=libraries" class="form">
	<div class="librerias config">
	<?php
	foreach($librerias as $key => $value){
		$selected = (in_array($value,$seleccionados))?"checked='checked'":""; ?>
		<div>
			<label for="<?php echo $key; ?>"><?php echo $key; ?></label>
			<input <?php echo $selected; ?> type="checkbox" id="<?php echo $key; ?>" name="library[<?php echo $key; ?>]" value="<?php echo $value; ?>" />
		</div>
	<?php }?>
	<div class="clear"></div>
	</div>
	
	<?php Form::submit("Guardar cambios"); ?>
</form>

