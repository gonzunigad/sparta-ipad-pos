<script type="text/javascript">
$(function(){
	$("#mod_name").keyup(function(){
		var text = this.value.toLowerCase().replace(/ /g,"_");
		text = text.replace("�","a").replace("�","e").replace("�","i").replace("�","o").replace("�","u").replace("�","n")
		document.getElementById("mod_code").value = text;
	});
});
</script>
<h2><?php ___("Crear m&oacute;dulo"); ?></h2>
<p id="page-intro"><?php ___("Al parecer tu sitio ya va tomando forma..."); ?></p>

<div class="content-box"><!-- Start Content Box -->
	
	<div class="content-box-header">
		<h3><?php ___("Nuevo m&oacute;dulo"); ?></h3>
		<div class="clear"></div>
	</div> <!-- End .content-box-header -->
	
	<div class="content-box-content">
		<div class="tab-content " id="tab2">
			<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" class="form">
				<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
					<?php Form::text("mod_name",array(
						"label" => "Nombre",
						"desc" => "Nombre que no se que..."
					)); ?>
					<?php Form::text("mod_code",array(
						"label" => "C&oacute;digo"
					)); ?>
					<p><input class="button" type="submit" value="<?php ___("Crear m&oacute;dulo"); ?>" /></p>
				</fieldset>
				<div class="clear"></div><!-- End .clear -->
				
			</form>
			
		</div> <!-- End #tab2 -->        
		
	</div> <!-- End .content-box-content -->
	
</div>