<?php
class ModController extends MainController{
	var $name = "Mod";
	var $model = array("Mod");
	
	function index(){
		
	}
	
	function add(){
		if($this->Mod->validate()){
			$this->Mod->insert();
		}else{
			echo json_encode(array(
				"error" 	=> true,
				"msg"		=> __("We have problems creating the module."),
			));
		}
	}
} ?>