<?php Messages(); ?>
<?php if($fields->num_results > 0){ ?>
<table>
	<thead>
		<tr>
			<th width="20"><input class="check-all" type="checkbox" /></th>
			<th><?php ___("Nombre"); ?></th>
			<th><?php ___("Tipo"); ?></th>
			<th><?php ___("Opciones"); ?></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="6"><div class="bulk-actions align-left">
					<select name="dropdown">
						<option value="option1">Choose an action...</option>
						<option value="option2">Edit</option>
						<option value="option3">Delete</option>
					</select>
					<a class="button" href="#">Apply to selected</a> </div>
				<div class="pagination"><?php echo $fields->paginador(); ?></div>
				<!-- End .pagination -->
				<div class="clear"></div></td>
		</tr>
	</tfoot>
	<tbody>
		<?php while($row = $fields->each()){  ?>
			<tr>
				<td><input type="checkbox" /></td>
				<td>
					<?php echo $row["fie_name"]; ?>
					(<strong><?php echo $row["fie_code"]; ?></strong>)
				</td>
				<td><?php echo $row["fie_type"]; ?></td>
				<td><?php Component::load("reg_options",array("id" => $row["mod_id"])); ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>
<?php }else{
	printMessage("No se han creado campos para este m&oacute;dulo. Puede comenzar a hacerlo en la pesta&ntilde;a 'Nuevo Campo'.","information");
}?>