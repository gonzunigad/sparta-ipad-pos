<?php

if(isset($_REQUEST['elim'])){
	$mod = Model::instance("Field");
	if($mod->delete($_REQUEST['elim'])){
		die("{estado:true}");
	}else{
		die("{estado:false}");
	}
}

if(isset($data)){
	$fields = Model::instance("field");
	$msj = "";
	if(trim($data[$fields->name]["fie_name"]) == "")$msj .= "Debe ingresar un nombre para el campo<br />";
	if(trim($data[$fields->name]["fie_code"]) == "")$msj .= "Debe ingresar un c&oacute;digo para el campo<br />";
	if($msj != "")die("{error:1,msg:'<strong>Solucione los siguientes problemas:</strong><br />$msj'}");
		
	if($_GET['type'] == "add"){
		$res = $fields->insert();
		if($res){
			$resp = "<tr>
				<td>".$data[$fields->name]["fie_name"]."</td>
				<td>".$data[$fields->name]["fie_code"]."</td>
				<td>".$data[$fields->name]["fie_type"]."</td>
			</tr>";
			echo json(array(
				"msg" 			=> 'Campo creado astisfactoriamente',
				"type"			=> "success",
				"allowsubmit"	=> "true",
				"clear"			=> "true",	
				"onFinish"		=> "reloadFields(\"$resp\")"
			));
			exit;
		}else{
			die("{error:1,msg:'Ocurri&oacute; un error al pocesar los datos.'}");		
		}
	}else{
		$fields->update();
	}
}
?>