<?php
$mod= Model::instance("Module");
$data[$mod->name] = $mod->getById($_GET['id']);
?>
<fw:container title="Editar m&oacute;dulo">
	<form action="panel/mods/save?type=edit" method="post" class="form">
		<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
			<?php Form::text("Module.mod_name",array(
				"label" => "Nombre",
				"desc" => "Nombre que no se que..."
			)); ?>
			<?php Form::text("Module.mod_code",array(
				"label" => "C&oacute;digo"
			)); ?>
			<?php Form::text("Module.mod_table",array(
				"label" => "Nombre de la tabla",
				"desc"	=> "Nombre de la tabla que se generar&aacute; en el sistema. Si ya existe, se intentar&aacute; usar el modelo actual"
			)); ?>
			<p><input class="button" type="submit" value="<?php ___("Crear m&oacute;dulo"); ?>" /></p>
		</fieldset>
		<div class="clear"></div><!-- End .clear -->
		
	</form>
</fw:container>