$(function(){
	$('.input.multi_image [type=file]').each(function(){
		var obj = this;
		var $input = $(obj).closest(".input");
		var $images = $input.find(".images");
		
		$(".delete").live("click",function(){
			if(!confirm("¿Esta seguro que desea eliminar este archivo?"))return false;
			var $im = $(this).closest(".image");
			$im.fadeOut(600,function(){
				$im.remove();	
			});
			
			return false;
		});
		
		//SORTABLE
		$input.find(".images").sortable({placeholder: "image placeholder",tolerance:'pointer'});
		
		
		$(".view").live("click",function(){
			var fname = $(this).parent().find("img").attr("src");
			$.facebox({image:fname});
			return false;
		});
		
		if($(this).data("uploadified") !== "true")$(this).data("uploadified","true").uploadify({
			'uploader'		: 'resources/js/uploadify/uploadify.swf',
			'script'		: 'fw-files/standalone/uploader.php',
			'folder'		: 'fw-files/fw-uploads/',
			'cancelImg'		: 'resources/js/uploadify/cancel.png',
			"wmode"			: "transparent",
			"auto"			: true,
			"multi"			: true,
			"onSelect"		: function(e){
				uploading_files++;
			},
			"onComplete"	: function(e,ID,file,response,data){ //http://www.uploadify.com/documentation/events/oncomplete-2/
				var resp = eval("("+response+")");
				queID = ID;
				if(resp.estado){
					finalizaArchivo("complete");
					new_image(UPLOADS_URL+resp.filename);	
				}else{
					addMessage(resp.msg,"error",$(e.target).closest("form")[0]);
					finalizaArchivo("error");	
				}
			},
			"onCancel"		: function(e){
				
			},
			"onError"		: function(a,b,c,d){
				finalizaArchivo("error");
			}
		});
		
		function new_image(img_filename){
			var img = $("<img />").attr("src",img_filename);
			var hidden = $("<input type='hidden'>").attr("name",$input.attr("name")+"[]").val(img_filename.substring(UPLOADS_URL.length));
			var $newdiv = $("<div />").addClass("image").html(img).append(hidden).append("<div class='delete ui-state-default ui-corner-all'><span class='ui-icon ui-icon-circle-close'></span></div>").append("<div class='view ui-state-default ui-corner-all'><span class='ui-icon ui-icon-arrow-4-diag'></span></div>").append("<div class='edit ui-state-default ui-corner-all'><span class='ui-icon ui-icon-pencil'></span></div>");
			$newdiv.hide()
			$images.append($newdiv);
			$newdiv.fadeIn();
		}
		$images.find("img").each(function(){
			var value = $(this).attr("src");
			new_image(value);
			$(this).remove();
		});
		function cancelImage($input){

		}
	});
});