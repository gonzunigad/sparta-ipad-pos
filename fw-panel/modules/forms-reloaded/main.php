<?php
$multi_images = 0;

$fw->addReloadJs(MODULES_DIR."forms-reloaded/res/ajax.reload.js");
$fw->addBaseJs(MODULES_DIR."forms-reloaded/res/jquery.cleditor.min.js");
$fw->addBaseCss(MODULES_DIR."forms-reloaded/res/jquery.cleditor.css");
$fw->addBaseCss(MODULES_DIR."forms-reloaded/res/fr.multi_image.css");

add_new("form_type","html","form_reloaded_html");
add_new("form_type","multi_image","form_reloaded_multi_image");

function form_reloaded_html($name, $valor,$input){
	global $page,$fw;
	$class = (isset($input->class))?$input->class:"";
	return "
	<div class='input html $class'>
		<label>$input->label</label>
		<textarea name='$name'>$valor</textarea>
	</div>";

}

function form_reloaded_multi_image($name,$valor,$input){
	global $multi_images;
	$multi_images++;
	$res = "<div name='$name' class='input oneline multi_image'><div class='wrapper'>
		<div class='controls'>
			<label>$input->label</label>
			<input type='file' id='multi_image$multi_images' />
		</div>
		<div class='images'>";
		if($valor){
			$r = explode(",",$valor);
			foreach($r as $image){
				$res.="<img src='".UPLOADS_URL."$image' alt='' />";
			}
		}
	$res .="
		<input type=\"hidden\" name=\"$name\" value=\"\" />
	</div>
		
	</div></div>";
	return $res;	
}
?>