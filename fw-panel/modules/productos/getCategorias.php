<?php
$mod = Model::instance("Categoria");
$mod->cond["cat_padre"] = $_REQUEST['cat_padre'];
$mod->order = array("cat_padre" => "desc");
$mod->select();
?>
<script>
$("#segundo select").change(function(){
	window.location.hash = getTargetUrl("list?cat="+this.value);
});
</script>

<div class="input select">
	<label>Subcategoría: </label>
	<select style="float:left; width:200px">
	<?php 
	while ($row = $mod->each()){
		echo "<optgroup label='$row[nombre]'>";
		$mod2 = Model::instance("Categoria");
		$mod2->cond["cat_padre"] = $row["id"];
		$mod2->listar("<option value='[id]'>[nombre]</option>");
		echo "</optgroup>";
	}
	?>
	</select>
</div>