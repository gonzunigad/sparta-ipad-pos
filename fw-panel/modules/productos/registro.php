<?php 
$isEdit = (isset($_GET['id']))?true:false; 
$verb = ($isEdit)?$mod_verb[0]:$mod_verb[1];
$action = ($isEdit)?"save?type=edit&id=$_GET[id]":"save?type=add";
if($isEdit){
	$mod= Model::instance($mod_model);
	$data[$mod_model] = $mod->getById($_GET['id']);	
	$data[$mod_model] = $data[$mod_model][$mod_model];
}
$cats = Model::instance("Categoria");
$categorias = $cats->getSimpleArray("nombre");

?>
<fw:title><?php ___("$verb ".strtolower($mod_object)); ?></fw:title>

<fw:container title="Formulario">
	<div class="form">
		<form action="<?php echo $action; ?>" method="post">
		<fw:input type="text" label="Nombre: " name="<?php echo $mod_model; ?>.nombre"></fw:input>
		<fw:input type="text" label="Modelo: " name="<?php echo $mod_model; ?>.modelo"></fw:input>
		<fw:input type="text" label="Precio:  (sin puntos ni coma)" name="<?php echo $mod_model; ?>.precio"></fw:input>
		<fw:input type="text" label="Stock: " name="<?php echo $mod_model; ?>.stock"></fw:input>
		<fw:input type="text" label="Video Youtube (código): " name="<?php echo $mod_model; ?>.youtube"></fw:input>
		<fw:input type="image" label="Imagen: " name="<?php echo $mod_model; ?>.imagen"></fw:input>
		<fw:input type="multi_image" label="Más imágenes " name="<?php echo $mod_model; ?>.galeria"></fw:input>
		<fw:input type="textarea" label="Detalle: " name="<?php echo $mod_model; ?>.detalle"></fw:input>
		<fw:input type="textarea" label="Características Técnicas: " name="<?php echo $mod_model; ?>.caracteristicas"></fw:input>
		<?php echo Form::select("$mod_model.activo",array("options" => array("No, estará oculto","Si, será visible"),"label" => "Activo: ")); ?>
		
		<?php echo Form::select("$mod_model.cat_id",array("options" => $categorias,"label" => "Categoría: ")); ?>
		
		<div class="input submit">
			<input type="submit" class="button" value="<?php ___("$verb ".strtolower($mod_object)); ?>" />
		</div>
		</form>
	</div>
</fw:container>