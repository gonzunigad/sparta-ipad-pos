<?php
class Producto extends MainModel{
    var $name = "Producto";
    var $table = "productos";
	var $belongsTo = array(
		"Categoria" => array(
			"foreignKey"	 => "cat_id"
		)
	);
}
?>