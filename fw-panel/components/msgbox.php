<?php //Type can be "attention","information","success" or "error" 
if(!isset($type))$type="information";
if(!isset($msg))$msg=__("Debe ingresar un mensaje...");
?>
<div class="notification <?php echo $type; ?> png_bg">
	<a href="#" class="close"><img src="<?php echo PANEL_URL; ?>resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
	<div>
		<?php ___($msg); ?>
	</div>
</div>