<a href="edit.php?id=<?php echo $id; ?>" title="Edit" class="edit">
	<img src="<?php echo PANEL_URL; ?>resources/images/icons/pencil.png" alt="Edit" />
</a>
<a href="change_state.php?id=<?php echo $id; ?>" title="Edit" class="edit">
	<img src="<?php echo PANEL_URL; ?>resources/images/icons/lightbulb.png" alt="Edit Meta" />
</a>
<a href="delete.php?id=<?php echo $id; ?>" title="Delete" class="delete">
	<img src="<?php echo PANEL_URL; ?>resources/images/icons/cross.png" alt="Delete" />
</a>