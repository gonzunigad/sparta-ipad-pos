<?php
$page->template = "install";
$page->title = __("Sign in")." | Freshwork";
?>
<script type="text/javascript">
$(function(){
	if( window.location.hash != "")document.getElementById("goto").value = window.location.hash.substring(2);	
});
$(function(){
	$("#user").focus();
});
</script>
<div id="login-wrapper" class="png_bg">
	<div id="login-top">
		<h1><?php ___("Sign in"); ?></h1>
		<p id="page-intro"><?php ___("Is it time to change the site?"); ?></p>
	</div>
	<!-- End #logn-top -->
	<div id="login-content">
		<?php if(isset($html_message))echo $html_message; ?>
		<form action="" method="post" class="form oneline">
			<fieldset>
				<input type="hidden" name="paso" value="1" />
				<!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
				<div class="input text">
					<label><?php ___("User"); ?>: </label>
					<input value="<?php if(isset($_POST["user"]))echo $_POST["user"]; ?>" id="user" type="text"  name="user" />
				</div>
				<div class="input text">
					<label><?php ___("Password"); ?>: </label>
					<input type="password" name="pwd" />
				</div>
				<input type="hidden" name="goto" id="goto" value="" />
				<div class="input submit">
					<input class="button" type="submit" value="<?php ___("Sign in"); ?>" />
				</div>
			</fieldset>
			<div class="clear"></div>
		</form>
	</div>
	<!-- End #login-content -->
</div>