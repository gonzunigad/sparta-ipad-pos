function getCurrentUrl(){
	var current_url = window.location.hash.substring(2);
	if(current_url.indexOf("?") === -1){
		current_url = current_url+"?";

	}else{
		if(current_url.indexOf("=") !== -1)current_url = current_url+"&";
	}
	return current_url;
}

function dirname(path){
	return path.replace(/\\/g, '/').replace(/\/[^\/]*\/?$/, '');
}

function parseURL(url,currentFolder){
	var tmp = url.indexOf("../");
	if(tmp >= 0){
		currentFolder = dirname(currentFolder);
		url = url.replace("../","");
		currentFolder = parseURL(url,currentFolder);	
	}else{
		return currentFolder;	
	}
	return currentFolder;
}

function getTargetUrl(url,putExclamation){
	if(typeof(putExclamation) == "undefined")putExclamation = true; 
	if(url[0] == "/"){			
		res = url.substring(1);
	}else{
		
		var current_folder = dirname(getCurrentUrl());
		current_folder = parseURL(url,current_folder);
		res =  current_folder +"/"+ url.replace("../","");
	}
	return (!putExclamation)?res:"!"+res;
}

function cargar(url, data,callback,hashchange){
	var time = ($.browser.msie)?0:300;
	//window.location.hash = "!"+url;
	$("#ajax_loading").fadeIn(time);
	var obj = document.getElementById("main-content");
	if(typeof(MAIN_XHR) != "undefined")MAIN_XHR.abort();
	MAIN_XHR = SoloEnviar(url,data,function(con){
		$(obj).fadeOut(time,function(){
			$(obj).html(con.responseText);
			$(obj).fadeIn(time);
			$("#ajax_loading").fadeOut(time);
			if(typeof(callback) == "function")callback(con.responseText,con);
		});
	});
}

function in_array (needle, haystack, argStrict) {
    var key = '', strict = !!argStrict;

    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle)return true;
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle)return true;
        }
    }

    return false;
}

function finalizaArchivo(type){
	if(type == "error"){
		msgbox("Ocurrió un error al subir uno o m&aacute;s archivos, por lo que el formulario no se envi&oacute;. <br />Revise los archivos que fallaron y vuelva a intentar.","error");
	}
	uploading_files--;
	if(uploading_files == 0 && submitOnFinishUpload !== false){
		$(submitOnFinishUpload).submit();
	}
}

function addMessage(msg,type,div,prepend){
    if(typeof(prepend) == "undefined")prepend = true;
    $div = $("<div>");
    if(prepend){
        $(div).prepend($div);
    }else{
        $(div).append($div);
    }
	$(document).trigger('freshwork.showMessage',[msg,type,div,prepend]);
    $div.html(msgbox(msg,type)).fadeIn();

}