var uploading_files = 0;
var submitOnFinishUpload = false;
var current_url = getCurrentUrl();


var MAIN_XHR;
if(window.location.hash.substring(1,2) == "!"){
	
	//Sistema deshabilitado
	//window.location = window.location.hash.substring(2);
	$(function(){cargar(window.location.hash.substring(2),"");});
}else{
	$(function(){cargar("home","",false,true);});
}
d = document;

//List variables
var order_params = "";
var direction_params = "";
var query_params = "";
var pagination_params = "";
function getListQuery(){
	return current_url+order_params+direction_params+query_params+pagination_params;
}



$(function(){
	$(window).bind('hashchange', function(e, newHash){
		if(window.location.hash.substring(1,2) == "!"){
			cargar(window.location.hash.substring(2),false,false,true);
			$("#main-nav a").each(function(){
				if(this.href == window.location){
					$(this).click();
				}
			});
		}
	});
	var direction = "ASC";
	$("table th[order]").live("click",function(){
		var order = $(this).attr("order");
		direction = (direction == "DESC" )?"ASC":"DESC";
		order_class = direction.toLowerCase();
		
		order_params = "order="+order+"&";
		direction_params = "direction="+direction+"&";
		var url = getListQuery();
		
		SoloEnviar(url,"ajaxpipe=1",function(con){
			$("#list_wrapper").html(con.responseText);
			$("table th[order="+order+"]").addClass(order_class);
		});
	});
	
	$(".fw_search").live("keyup",function(){
		var txt = $(this).val();
		var tar = $(this).attr("target");
		var target=(typeof(rel) == "undefined")?"#list":tar;
		
		if(typeof(XHR) != "undefined" && XHR !== false)XHR.abort();
		query_params = "q="+txt;
		var url = getListQuery();
		
		XHR = SoloEnviar(url,"ajaxpipe=1",function(con){
			var $div = $("<div>");
			$div.html(con.responseText);
			$dentro = $div.find(target).children();
			$(target).html($dentro);
		});
	});
	
	//Subida de archivos
	$(".form .file .cancel a").live("click",function(){
		$(this).parent().parent().fadeOut("normal",function(){
			$(this).parent().parent().find("object").css("height","30px");
			$(this).remove();
		});
		$(this).parent().parent().parent().parent().find("[type=hidden]").val("");
	});
	
	//OPTIONS
	$(".option_state").live("click",function(e){
		var obj = e.target;
		var tmp = obj.style.color;
		var tmp2 = obj.innerHTML;
		obj.style.color = "blue";
		var state = $(obj).attr("state");
		var new_state = (state == "1")?"0":"1";
		SoloEnviar(getTargetUrl($(this).attr("href"),false),"output=json&set_state="+state,function(con){
			var resp = eval("("+con.responseText+")");
			if(resp.estado){
				$(obj).html(resp.btn_msg);
				$(obj).attr("state",new_state);
			}else{
				alert("No se ha podido cambiar el estado del registro.");
				obj.innerHTML = tmp2;
			}
			obj.style.color = tmp;
		});
		return false;
	});
	$(".option_delete").live("click",function(e){
		if(!confirm("\u00BFEstas seguro que deseas eliminar este registro?"))return false;
		var obj = e.target;
		var tmp = obj.style.color;
		obj.style.color = "blue";
		
		SoloEnviar(getTargetUrl($(this).attr("href"),false),"output=json",function(con){
			var resp = eval("("+con.responseText+")");
			if(resp.estado){
				$(obj).closest("tr").fadeOut();
			}else{
				alert("Ocurrió un error al procesar la eliminación. Intenta más tarde");
				obj.style.color = tmp;
			}
		});
		return false;
	});
	
	
	
	/*$(".input.file .delete").live("click",function(){
		if(!confirm("¿Esta seguro que desea eliminar este archivo?"))return false;
		
		$(this).parent().find("input").val("");
		$(this).parent().find(".current").fadeOut();
		$(this).fadeOut();
		return false;
	});*/
	
	$(".radios .radio").live("click",function(e){
		$(this).addClass("selected").siblings().removeClass("selected");
		$(this).find("input").attr("checked","checked");
	});
	
	$("form [type=reset]").live("click",function(event){
		if(typeof($(this).attr("href")) == "undefined"){
			window.history.go(-1);
		}else{
			window.location.href = "#!"+$(this).attr("href");
		}
	});
	
	var tmp_txt = false;
	$("form").live("submit",function(event){
		var obj = this;
		var subm = $(this).find("[type=submit]");
		if(tmp_txt == false)tmp_txt = subm[0].value;
		
		if(uploading_files > 0){
			if(submitOnFinishUpload == false){
				subm[0].value = ("Esperando subida de archvios para enviar - Clic para cancelar");
				submitOnFinishUpload = this;
			}else{
				subm[0].value = (tmp_txt);
				submitOnFinishUpload = false;
			}
			return false;	
		}
		
		subm[0].value = "Enviando datos...";
		subm[0].disabled = true;
		subm.addClass("disabled");
		var type = $(obj).attr("default");
		if(typeof(type) == "undefined" || type != "true"){
			subm[0].blur();
			SoloEnviar(getTargetUrl($(obj).attr("action"),false),"output=json&"+$(this).serialize(),function(con){
				subm.removeClass("disabled");
				var resp;
				try{
					resp = eval("(" + con.responseText + ")");
				}catch(err){
					resp = {'error':true,'msg':'Ocurri&oacute; un error al procesar la respuesta <a class="noajax" target="_blank" href="http://en.wikipedia.org/wiki/JSON">json</a> del servidor.<br /><strong>P&aacute;gina:</strong> '+$(obj).attr("action")+'<br /><strong>Error del sistema: </strong>'+err+'<br /><strong>Respuesta sin formato:</strong> "'+con.responseText+'"'}
				}
				if(resp.error && typeof(resp.type) == "undefined")resp.type="error";
				if(typeof(resp.type) == "undefined")resp.type = "information";
				if(typeof(resp.showMessage) == "undefined")resp.showMessage = true;
				if(typeof(resp.msgpos) == "undefined")resp.msgpos = "top";
				if(typeof(resp.autohide) == "undefined")resp.autohide = false;
				if(typeof(resp.replaceMsg) == "undefined")resp.replaceMsg = true;
				
				//Redirecccionar la página si fué solicitado
				if(typeof(resp.redirect_to) != "undefined"){
					window.location.hash = "!"+resp.redirect_to;
					return;
				}
				
				if(resp.showMessage){
					//Muestra el mensaje
					
					var div = $("<div>");
					div.html(msgbox(resp.msg,resp.type));
					div.hide();
					var tiempo = 0;
					if(resp.replaceMsg){
						//var count = $(".notification .close").length;
						//$(".notification .close").click();
						//if(count > 0)tiempo = 1000		
						$(".notification").hide(100);
					}
					
					setTimeout(function(){
						if(resp.msgpos == "top"){
							$(obj).prepend(div);
						}else if(resp.msgpos == "bottom"){
							$(obj).append(div);
						}else if(resp.msgpos == "both"){
							var div2 = div.clone();
							$(obj).prepend(div); 
							$(obj).append(div2);
						}
					
						if(resp.autohide != false){
							setTimeout(function(){
								div.fadeOut();
							},resp.autohide*1000);
						}
						div.show("normal");
						$(document).trigger('freshwork.showFormMessage',[resp.msg,resp.type,obj]);
						if(typeof(div2) != "undefined")div2.show("normal");
					},tiempo);
				}
				
				if(typeof(resp.clear) != "undefined" && resp.clear == true){
					clear_form_elements(obj);
				}
				
				//Lógica a seguir si el resultado fue un error o no
				if(typeof(resp.error) == "undefined" || resp.error == false){
					//Si no hubieron errores
					if(typeof(resp.allowsubmit) == "undefined")resp.allowsubmit = false;
					subm[0].value = "Guardar cambios";
					/*subm[0].value = "¡Formulario enviado satisfatoriamente!";
					if(resp.allowsubmit)subm[0].value +=  " - Volver a enviar.";*/
				}else{
					if(typeof(resp.allowsubmit) == "undefined")resp.allowsubmit = true;
					subm[0].value = "Guardar cambios nuevamente";
				}
				//Indica si se podrá o no reenviar el formulario
				if(resp.allowsubmit){
					subm[0].disabled = false;
				}else{	
					subm.addClass("disabled");
				}
				
				if(typeof(resp.onFinish) != "undefined"){
					eval(resp.onFinish);
				}
			});
		}else{
			cargar($(this).attr("action"),$(this).serialize());	
			//if(!$.browser.msie)window.location.hash += "#datos_enviados";
		}
		
		
		/*;*/
		return false;
	});
	
	$('.check-all').live("click",function(){
		$(this).parent().parent().parent().parent().find("input[type='checkbox']").attr('checked', $(this).is(':checked'));
	});
	
	/* TAGS */
	$(".multiselect .option .close").live("click",function(){
		var obj = this;
		$(obj).parent().hide(250,function(){
			$(obj).parent().remove();
		});
	});
	
});

function formatItem(row) {
	return row[1];
}
function formatResult(row) {
	return row[1];//.replace(/(<.+?>)/gi, '');
}

function setCookie(c_name,value,expiredays){
	var exdate=new Date();
	exdate.setDate(exdate.getDate()+expiredays);
	document.cookie=c_name+ "=" +escape(value)+
	((expiredays==null) ? "" : ";expires="+exdate.toUTCString());
}
function getCookie(c_name){
	if(document.cookie.length>0){
		c_start=document.cookie.indexOf(c_name + "=");
	  	if (c_start!=-1){
			c_start=c_start + c_name.length+1;
			c_end=document.cookie.indexOf(";",c_start);
			if (c_end==-1) c_end=document.cookie.length;
			return unescape(document.cookie.substring(c_start,c_end));
		}
	}
	return "";
}
function msgbox(str,type){
	var tpl = getCookie("fw_msgbox_tpl");
	return tpl.replace(/\[MSG]/ig,str).replace(/\[TYPE]/ig,type).replace(/\+/g," ");
}

function clear_form_elements(ele){
    $(ele).find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });

}

function deselectMenu(){
	$("#main-nav a").removeClass("current");	
}