function AJAXPOST(Pagina, Variables,Obj, MsjLoad, FuncionListo, FuncionCarga, Conexion){
	if (MsjLoad == null || MsjLoad == false){MsjLoad= MsjTipico;}
	var Conexion = crearXMLHttpRequest();
	Conexion.open("POST",Pagina, true);
	Conexion.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	Conexion.setRequestHeader("X-Requested-With","XMLHttpRequest");
	Conexion.send(Variables);
	
	Conexion.onreadystatechange = ProcesarCambioEstado;
		function ProcesarCambioEstado(){
			if (Conexion.readyState == 4){
				if(Obj != false){
					$(Obj).fadeOut("fast",function(){$(Obj).html(Conexion.responseText);});
					$(Obj).fadeIn("normal",function(){
						if (FuncionListo != null && FuncionListo != false){
							x = FuncionListo;
							x(Conexion);
						}else{
							if(typeof(carga_pagina) == "function")carga_pagina();	
						}
					});
				}else{
					if (FuncionListo != null){
						x = FuncionListo;
						x(Conexion);
					}
				}
				
			}else{
				if (FuncionCarga == null){
					//if(Obj != false){Obj.innerHTML = MsjLoad;}
				}else{
					x = FuncionCarga;
					x(Conexion);
				}
			}
		}
	return Conexion;
}

function SoloEnviar(Pagina, Variables, Callback){
	var Conexion = crearXMLHttpRequest();
	if(typeof(Variables) == "undefined")Variables = "";
	Conexion.open("POST",Pagina, true);
	Conexion.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	Conexion.setRequestHeader("X-Requested-With","XMLHttpRequest");
	Conexion.send(Variables);
	Conexion.onreadystatechange = function(){
		if(Conexion.readyState == 4){
			if(Callback != null){
				x = Callback;
				x(Conexion);
			}
		}
	}
	return Conexion;
}

function crearXMLHttpRequest(){
  var xmlHttp=null;
  if (window.ActiveXObject) 
    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
  else 
    if (window.XMLHttpRequest) 
      xmlHttp = new XMLHttpRequest();
  return xmlHttp;
}
//FIN FUNCUINES AJAX