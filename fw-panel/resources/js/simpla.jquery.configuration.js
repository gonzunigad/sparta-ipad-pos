$(function(){
	//Sidebar Accordion Menu:
		$("#main-nav li ul").hide(); // Hide all sub menus
		$("#main-nav li a.current").parent().find("ul").slideToggle("slow"); // Slide down the current menu item's sub menu
		
		$(".nav-top-item:not(.no-submenu)").live("click", // When a top menu item is clicked...
			function () {
				$(this).parent().siblings().find("ul").slideUp("normal"); // Slide up all sub menus except the one clicked
				$(this).next().slideToggle("normal"); // Slide down the clicked sub menu
				return false;
			}
		);
		
		$("#main-nav li li a").live("click",function(){
			var obj = this;
			var href = $(this).attr("href");
			var data = $(this).attr("data");
			/*cargar(href,data,function(){
				$("#main-nav li li a").removeClass("current");
				$("#main-nav li a").removeClass("current");
				$(obj).addClass("current");	
				$(obj).parent().parent().parent().find("a.nav-top-item").addClass("current");	
			});*/
			$("#main-nav li li a").removeClass("current");
			$("#main-nav li a").removeClass("current");
			$(obj).addClass("current");	
			$(obj).parent().parent().parent().find("a.nav-top-item").addClass("current").next().slideDown().parent().siblings().find("ul").slideUp("normal");
			window.location = href;
			return false;
		});
		
		$(".no-submenu").live("click", // When a menu item with no sub menu is clicked...
			function () {
				var obj = this;
				var href = $(this).attr("href");
				var data = $(this).attr("data");
				deselectMenu();
				$(obj).addClass("current");
				$(this).parent().siblings().find("ul").slideUp("normal");
				window.location = href;
				return false;
			}
		); 

    // Sidebar Accordion Menu Hover Effect:
		
		$("#main-nav li .nav-top-item").hover(
			function () {
				$(this).stop().animate({ paddingRight: "25px" }, 200);
			}, 
			function () {
				$(this).stop().animate({ paddingRight: "15px" });
			}
		);

    //Minimize Content Box
		
		$(".content-box-header h3").css({ "cursor":"s-resize" }); // Give the h3 in Content Box Header a different cursor
		$(".closed-box .content-box-content").hide(); // Hide the content of the header if it has the class "closed"
		$(".closed-box .content-box-tabs").hide(); // Hide the tabs in the header if it has the class "closed"
		
		$(".content-box-header h3").live("click", // When the h3 is clicked...
			function () {
			  $(this).parent().next().slideToggle(); // Toggle the Content Box
			  $(this).parent().parent().toggleClass("closed-box"); // Toggle the class "closed-box" on the content box
			  $(this).parent().find(".content-box-tabs").toggle(); // Toggle the tabs
			}
		);

    // Content box tabs:
		$('.content-box ul.content-box-tabs li a').live("click",function(e) {  // When a tab is clicked...
			$(this).addClass('current').parent().siblings().find("a").removeClass('current');
			var currentTab = $(e.target).parent().parent().parent().next().find(".tab-content");
			var href = $(e.target).attr("href"); // Set variable "currentTab" to the value of href of clicked tab
			SoloEnviar(href,"",function(con){
				var TabOnLoad = $(e.target).attr("ontabload");
				currentTab.html(con.responseText);
				if(TabOnLoad != "")eval(TabOnLoad);
			});
			setCookie("current-tab",href);
			return false; 
		});

    //Close button:
		
		$(".notification .close").live("click",
			function () {
				$(this).parent().fadeTo(400, 0, function () { // Links with the class "close" will close parent
					$(this).slideUp(400);
				});
				return false;
			}
		);

    // Alternating table rows:
	$(".options").live("click",function(){
										
	});
	
	$("#main-nav a").each(function(){
		if(this.href == window.location){
			$(this).click();
		}
	});
});
  
  
  