(function($){
	var handleClick = true;
	var fromSB = false;
	Shadowbox.init({
		handleOversize:"none",
		enableKeys:	false	
	});
	
	var div1 = $("<div />").addClass("fw_pos1")[0];
	var div2 = $("<div />").addClass("fw_pos2")[0];
	var div3 = $("<div />").addClass("fw_pos3")[0];
	var div4 = $("<div />").addClass("fw_pos4")[0];
	$(function(){
		$("body").append(div1);
		$("body").append(div2);
		$("body").append(div3);
		$("body").append(div4);
		
		//SITE ADMIN
		$(document).bind('mousemove',function(e){
			if(Shadowbox.isOpen())return;
			var elem = (typeof(e.target) == "undefined")?e.srcElement:e.target;
			//if (!$(elem).hasClass("fw_list"))return;
			putWrapper(elem);
		}).bind("click",function(e){
			if(Shadowbox.isOpen())return;
			//$(document).unbind('mousemove');
			if(!e.ctrlKey)return true;
			var elem = (typeof(e.target) == "undefined")?e.srcElement:e.target;
			if(elem.id == "sb-overlay")return;
			
			var hasTemplate = ($("#fw_begin_markup").length > 0); //Verifica si se creo o no 
			var container = (hasTemplate)?$("#fw_begin_markup"):$("body");
			
			//We create a css-like selector based on the HTML element clicked.
			var parents = $(elem,container).parents();
			parents_array = [];
			var encontro = false;
			var selector = "";
			parents.each(function(){
				if(this == container[0])encontro = true;
				if(encontro == false)parents_array.unshift(this);
			});
			for (var key in parents_array){
				var obj = parents_array[key];
				clase = getClasses(obj);
				selector += " "+obj.tagName.toLowerCase()+clase;
			}
			var selector_complete = selector+" "+elem.tagName.toLowerCase();
			selector_complete += getClasses(elem)//.className;
			//Finish the creation of the css-like selector
			
			//Based om the CSS-like selector, we get the index of the current element that match wih the selector.
			var index = $(selector_complete).index(elem);
	
	
			//Si no encontro el tag container, es porque se encuentra sobre el, por lo que el tag no se encuentra dentro del archivo local, si no que del template.
			var hasInTempalte = hasTemplate && !encontro;
		
			
			var data = {
				"pageID":PAGEID,
				"pageTitle":document.title,
				"page":REQUEST_FILE,
				"index":index,
				"selector":selector_complete,
				"hasInTempalte":hasInTempalte, 
				"hasTemplate":hasTemplate, 
				"tag":elem.tagName, 
				"id":elem.id, 
				"class":elem.className,
				"html":elem.innerHTML};
			
			Shadowbox.open({
				content:    MODULES_URL+"freshwork/siteAdmin/click.php?shadowbox=1&"+$.param(data),
				player:     "iframe",
				title:      "Freshwork Admin",
				height:     450,
				width:      600
			});
			
			return false;
		});
	});
	function getClasses(elem){
		var tmp ="";
		var clases = elem.className.split(" ");
		for(var i in clases){
			if(clases[i] != ""){
				tmp += "."+clases[i];
				break;
			}
		}
		return tmp;
	}
	function putWrapper(elem){
		var pos = $(elem).offset();
		var alto = $(elem).innerHeight();
		var ancho = $(elem).innerWidth();
		
		if(elem.tagName == "FORM"){
			$(".fw_pos1,.fw_pos2,.fw_pos3,.fw_pos4").addClass("fw_edit_form");
		}else{
			$(".fw_pos1,.fw_pos2,.fw_pos3,.fw_pos4").removeClass("fw_edit_form");
		}
		//$(e.srcElement).bind("mouseout",function(){
		//	div.style.left="0px";
		//	div.style.width="0px";
		//});
		div2.innerHTML = elem.innerHTML;
		div1.style.left=pos.left+"px";
		div1.style.top = pos.top+"px";
		div1.style.width = "0px";
		div1.style.height = alto+"px";
		
		div2.style.left=pos.left+"px";
		div2.style.top = pos.top+"px";
		div2.style.width = ancho+"px";
		div2.style.height = "0px";
		
		div3.style.left=pos.left+ancho+"px";
		div3.style.top = pos.top+"px";
		div3.style.width = "0px";
		div3.style.height = alto+"px";
		
		div4.style.left=pos.left+"px";
		div4.style.top = pos.top+alto+"px";
		div4.style.width = ancho+"px";
		div4.style.height = "0px";
	}
	function URLEncode (clearString) {
	  var output = '';
	  var x = 0;
	  clearString = clearString.toString();
	  var regex = /(^[a-zA-Z0-9_.]*)/;
	  while (x < clearString.length) {
		var match = regex.exec(clearString.substr(x));
		if (match != null && match.length > 1 && match[1] != '') {
			output += match[1];
		  x += match[1].length;
		} else {
		  if (clearString[x] == ' ')
			output += '+';
		  else {
			var charCode = clearString.charCodeAt(x);
			var hexVal = charCode.toString(16);
			output += '%' + ( hexVal.length < 2 ? '0' : '' ) + hexVal.toUpperCase();
		  }
		  x++;
		}
	  }
	  return output.replace("/n","").replace("/r","").replace("/r/n","");
	}
})(jQuery);


function siteAdmin(){
	this.save = function(selector, index, html){
		$(selector)[index].innerHTML = (html);	
	}
}

var siteAdmin = new siteAdmin();