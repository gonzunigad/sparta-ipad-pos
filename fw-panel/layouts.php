<style type="text/css">
	.browsers{overflow:hidden; }
	.browsers, .browsers li{list-style:none; margin:0 !important; padding:0 !important; background:none  !important; }
	.browsers li{width:25%; text-align:center; float:left; }
	.browsers li span{display:block; height:100%; font-size:16px; margin-bottom:10px; }
	.browsers li img{width:70%; }
	.browsers li .button{width:50%; margin:0 auto; }
	.quotes{text-align:center; margin-top:30px; }
	quote{margin-top:10px; font-size:25px;  }
</style>
<fw:container title="<?php ___("Upgrade with the option that best suits you."); ?>">
	<ul class="browsers">
		<li>
			<span>Google Chrome</span>
			<img src="<?php echo RESOURCES_URL; ?>images/browsers/Chrome.png" />
			<a target="_blank"  href="http://www.google.com/chrome" class="noajax button"><?php ___("Download"); ?></a>
		</li>
		<li>
			<span>Firefox</span>
			<img src="<?php echo RESOURCES_URL; ?>images/browsers/Firefox.png" />
			<a target="_blank"  href="http://www.mozilla.com/firefox/" class="noajax button"><?php ___("Download"); ?></a>
		</li>
		<li>
			<span>Safari</span>
			<img src="<?php echo RESOURCES_URL; ?>images/browsers/Safari.png" />
			<a target="_blank"  href="http://www.apple.com/safari" class="noajax button"><?php ___("Download"); ?></a>
		</li>
		<li>
			<span>Opera</span>
			<img src="<?php echo RESOURCES_URL; ?>images/browsers/Opera.png" />
			<a target="_blank" href="http://www.opera.com/download/" class="noajax button"><?php ___("Download"); ?></a>
		</li>
	</ul>
	<div class="quotes">
		<quote></quote>
	</div>
</fw:container>