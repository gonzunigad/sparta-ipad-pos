<?php
$lang_name = "Chino";
$lang = array(
	"Instalación de FreshWork"		=> "Instalaci&oacute;n de FreshWork Admin",
	"Hello, %s"						=> "您好, %s",
	"View the Site"					=> "Ver el sitio",
	"Sign Out"						=> "Cerrar sesi&oacute;n",
	
	/* Nuevo producto */
	"New Product"					=> "Nuevo poducto",
	"Edit Product"					=> "Editar Producto",
	"It has never been so easy to edit products."	=> "Nunca fué tan fácil administrar productos",
	
	"<< Comienzo"					=> "<<",
	"< Anterior"					=> "<",
	"Siguiente >"					=> ">",
	"Final >>"						=> ">>"
);
?>