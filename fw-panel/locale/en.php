<?php
$lang_name = "English";
$lang = array(
	//LOGIN
	"Javascript disabled"					=> "Javascript is disabled or is not supported by your browser. Please <a href=\"%s\" title=\"Upgrade to a better browser\">upgrade</a> your browser or <a href=\"%s\" title=\"Enable Javascript in your browser\">enable</a> Javascript to navigate the interface properly.",
	
	"Instalación de FreshWork"				=> "Freshwork installation",
	"Instalar"								=> "Freshwork Admin installation",
);
?>