// JavaScript Document
var basedomain = "secure.freshwork.co";
var baseurl = "//secure.freshwork.co/apps/sparta/ipad/";
function DB(code,version,name, size){
	var me = this;
	me.name = name;
	me.version = version;
	me.code = code;
	me.size = size;
	me.dbobj = null;
	me.currentversion = null;
	me.onready = function(){};
	
	me.connect = function(){
		me.db = openDatabase(me.code, me.version,me.name,me.size);
	}
	me.setVersion = function(ver){
		me.currentversion = ver;
	}
	
	me.getLastSync = function(){
		return (typeof(localStorage["db.lastsync"]) == "undefined")?false:localStorage["db.lastsync"];
	}	
	me.setLastSync = function(ts){
		localStorage["db.lastsync"] = ts;
	}
	
	me.checkIfInstalled = function(){
		if(typeof(localStorage["db.version"]) == "undefined"){
			return false;
		}else{
			return (me.currentversion == localStorage["db.version"]);
		}
	}
	me.install = function(callback){
		me.db.transaction(function (tx) {
			//Categorías
			tx.executeSql('DROP TABLE IF EXISTS categorias');
			tx.executeSql('CREATE TABLE `categorias` (id INTEGER PRIMARY KEY AUTOINCREMENT,`cat_padre` int(11),`nombre` varchar(255),`imagen` varchar(255),`activo` tinyint(1) NOT NULL)',[]);
			tx.executeSql('DROP TABLE IF EXISTS tiendas');
			
			/* Tiendas */
			tx.executeSql('DROP TABLE IF EXISTS tiendas');
			tx.executeSql('CREATE TABLE `tiendas` (`id` int(11) NOT NULL,`nombre` varchar(255) NOT NULL,`jefe_tienda` varchar(255) NOT NULL,`telefono` varchar(255) NOT NULL,`direccion` varchar(255) NOT NULL,`mails_confirmacion` text NOT NULL,`mails_cotizacion` text NOT NULL,`password` varchar(255) NOT NULL)',[]);
			
			/* Productos */
			tx.executeSql('DROP TABLE IF EXISTS productos');
			tx.executeSql('CREATE TABLE `productos` (id INTEGER PRIMARY KEY AUTOINCREMENT,`nombre` varchar(255),`modelo` varchar(255),`imagen` varchar(255),`galeria` text,`detalle` text,`caracteristicas` text,`cat_id` int(11),`precio` int(11),`stock` int(11),`youtube` varchar(255),`activo` tinyint(1), permitir_compra tinyint(1) )',[]);
			
			/* Sub-Productos */
			tx.executeSql('DROP TABLE IF EXISTS subproductos');
			tx.executeSql('CREATE TABLE `subproductos` (id INTEGER PRIMARY KEY AUTOINCREMENT,parent_id int(11),`sku` int(11),`upc` varchar(50),`detalle` text,`activo` tinyint(1))',[]);
			
			
			
			/* Cotizaciones */
			tx.executeSql('DROP TABLE IF EXISTS cotizaciones');
			tx.executeSql('CREATE TABLE cotizaciones (id INTEGER PRIMARY KEY AUTOINCREMENT, equipo_id int(11), subproducto varchar(255), tienda_id int(11), nombre varchar(255), direccion varchar(255),comuna varchar(255), ciudad varchar(255), email varchar(255), rut varchar(15), genero varchar(100))',[]);
			
			/* Confirmaciones */
			tx.executeSql('DROP TABLE IF EXISTS confirmaciones');
			tx.executeSql('CREATE TABLE confirmaciones (id INTEGER PRIMARY KEY AUTOINCREMENT, equipo_id int(11), tienda_id int(11), nombre varchar(255), direccion varchar(255),comuna varchar(255), ciudad varchar(255), email varchar(255), rut varchar(15), genero varchar(100), tipo_documento varchar(20), numero_documento int(11), direccion_despacho varchar(255), razon_social varchar(255),giro varchar(255),rut_factura varchar(20),fecha_entrega date, telefono varchar(50), cantidad int(11))',[]);
			
			if(typeof(callback) == "function")callback();
			
			localStorage["db.version"] = me.currentversion;
			me.setLastSync(0);
		},  function(error) {  
            console.log( error.message);  
        });
	}
	
	me.checkIfNeedSync = function(){
		
	}
	me.syncUpload = function(callback){
		if(!navigator.onLine){
			if(typeof(callback) == "function")callback(false);
			return false;	
		}
		var needSync = false;
		//Enviar datos que se ingresaron sin conexión
		me.selectSql("SELECT * FROM cotizaciones",[],function(data){
			var cotizaciones = data;
			me.selectSql("SELECT * FROM confirmaciones",[],function(confirmaciones){
				if(cotizaciones.length > 0 || confirmaciones.length > 0)needSync=true;
				var cotizacionestxt = "";
				for(var i in cotizaciones){
					var val = cotizaciones[i];
					cotizacionestxt += "equipo_id[]="+val.equipo_id+"&subproducto[]="+val.subproducto+"&tienda_id[]="+val.tienda_id+"&nombre[]="+val.nombre+"&direccion[]="+val.direccion+"&comuna[]="+val.comuna+"&ciudad[]="+val.ciudad+"&email[]="+val.email+"&rut[]="+val.rut+"&genero[]="+val.genero+"&";
				}
				var confirmacionestxt = "";
				for(var i in confirmaciones){
					var val = confirmaciones[i];
					confirmacionestxt += "equipo_id[]="+val.equipo_id+"&tienda_id[]="+val.tienda_id+"&nombre[]="+val.nombre+"&direccion[]="+val.direccion+"&comuna[]="+val.comuna+"&ciudad[]="+val.ciudad+"&email[]="+val.email+"&rut[]="+val.rut+"&genero[]="+val.genero+"&tipo_documento[]="+val.tipo_documento+"&numero_documento[]="+val.numero_documento+"&direccion_despacho[]="+val.direccion_despacho+"&razon_social[]="+val.razon_social+"&giro[]="+val.giro+"&rut_factura[]="+val.rut_factura+"&fecha_entrega[]="+val.fecha_entrega+"&telefono[]="+val.telefono+"&cantidad[]="+val.cantidad+"&";
				}
				
				if(!needSync){		
					if(typeof(callback) == "function")callback(false); return;
				 }
				
				$.post("syncUpload.php?type=cotizacion",cotizacionestxt,function(res){
					if(res == "true"){
						me.db.transaction(function (tx) {
							//Categorías
							tx.executeSql('DELETE FROM cotizaciones');
						});
					}
					$.post("syncUpload.php?type=confirmacion",confirmacionestxt,function(res){
						if(res == "true"){
							me.db.transaction(function (tx) {
								//Categorías
								tx.executeSql('DELETE FROM confirmaciones');
							});
						}
						if(typeof(callback) == "function")callback(true);
						
					});
				});
				
			},true);
		},true);	
	}
	
	
	/*********** SYNC *****************/
	
	
	me.sync = function(callback){
		if(!navigator.onLine){
			if(typeof(callback) == "function")callback(false);
			return false;	
		}
		$.post(baseurl+"sync.php",{lastsync:me.getLastSync},function(res){
			var resp = eval("("+res+")");
			if(resp.synced){
				me.setLastSync(resp.lastchange);
				
				
				//Sincronizar categorias
				me.db.transaction(function (tx) { //TX1
					tx.executeSql('DELETE FROM categorias');
					for(var k in resp.categorias){
						var val = resp.categorias[k];
						tx.executeSql('INSERT INTO `categorias` (id,cat_padre,nombre ,imagen,activo) VALUES (?,?,?,?,?)',[val.id,val.cat_padre,val.nombre,val.imagen,val.activo]);
					}
					
					
					//Sincronizar produtos
					me.db.transaction(function (tx) { //TX2
						tx.executeSql('DELETE FROM productos');
						for(var k in resp.productos){
							var val = resp.productos[k];
							tx.executeSql('INSERT INTO `productos` (id,cat_id, nombre , modelo, imagen, galeria, detalle, caracteristicas,precio, stock, youtube, activo,permitir_compra) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)',[val.id,val.cat_id,val.nombre,val.modelo,val.imagen,val.galeria,val.detalle,val.caracteristicas,val.precio,val.stock,val.youtube,val.activo,val.permitir_compra]);
						}
						//Sincronizar subprodutos
						me.db.transaction(function (tx) { //TX3
							tx.executeSql('DELETE FROM subproductos');
							//console.log(resp);
							for(var k in resp.subproductos){
								var val = resp.subproductos[k];
								tx.executeSql('INSERT INTO `subproductos` (id,parent_id,sku, upc, detalle,activo) VALUES (?,?,?,?,?,?)',[val.id,val.parent_id,val.sku,val.upc,val.detalle,val.activo]);
							}
							//Sincronizar tiendas
							me.db.transaction(function (tx) { //TX4
								tx.executeSql('DELETE FROM tiendas');
								for(var k in resp.tiendas){
									var val = resp.tiendas[k];
									tx.executeSql('INSERT INTO `tiendas` (id, nombre , jefe_tienda, telefono, direccion, mails_confirmacion, mails_cotizacion, password) VALUES (?,?,?,?,?,?,?,?)',[val.id,val.nombre,val.jefe_tienda,val.telefono,val.direccion,val.mails_confirmacion,val.mails_cotizacion,val.password]);
								}
								
								if(typeof(callback) == "function")callback(true);
							},  function(error) {  
								//IF ERROR IN TX4
								alert( error);  
							});
						});
						
					},  function(error) {  
						//IF ERROR IN TX2
						console.log( error );  
					});
				});
				
				localStorage["password_envio"] = resp.pass; //Deprecated
				
			}else{
				if(typeof(callback) == "function")callback(false);
			}
		});
	}
		
	me.selectSql = function(sql, data, callback,toArray,failback){
		me.db.transaction(function(tx){
			tx.executeSql(sql, data, function (tx, results) {
				if(typeof(toArray) == "undefined" || toArray == false){
					callback(results);
				}else{
					var tmp = [];
					var len = results.rows.length, i;
					for (i = 0; i < len; i++) {
						tmp.push(results.rows.item(i));
					} 
					callback(tmp);
				}

			},function (tx, error) {
				if(typeof(failback) == "function")failback(e,rror);
			});
		});
	
		return [];
	}
	
	//Init process
	me.connect();
}
