<?php
header("Content-Type: text/cache-manifest");
$v = "2.9";
$page->template = false;
$page->put_additional_content = false;
function getFilesFromDir($dir) { 

  $denied = array("fw-panel","manifest.cache","error_log","mail");
  $files = array(); 
  if ($handle = opendir($dir)) { 
    while (false !== ($file = readdir($handle))) { 
        if ($file != "." && $file != ".." && substr($file,-4) != ".php" && substr($file,0,1) != "." && substr($file,0,1) != "_" && !in_array($file,$denied)) { 
            if(is_dir($dir.'/'.$file)) { 
                $dir2 = $dir.'/'.$file; 
                $files[] = getFilesFromDir($dir2); 
            } 
            else { 
              $files[] = $dir.'/'.$file; 
            } 
        } 
    } 
    closedir($handle); 
  } 

  return array_flat($files); 
} 
function array_flat($array) { 
	$tmp = array();
  foreach($array as $a) { 
    if(is_array($a)) { 
      $tmp = array_merge($tmp, array_flat($a)); 
    } 
    else { 
      $tmp[] = $a; 
    } 
  } 

  return $tmp;
} 



header("Content-type: text/cache-manifest");
// Usage 
$dir = '.'; 
$foo = getFilesFromDir($dir); 

$mod = Model::instance("Categoria");
$arr = $mod->getSimpleArray("imagen");

$mod2 = Model::instance("Producto");
$mod2->returnFormat = "array";
$arr2 = $mod2->select();


//Aca falta añadir las imagenes usadas

$files = array();
foreach($arr2 as $valor){
	$galeria = $valor["Producto"]["galeria"];
	$valor = $valor["Producto"]["imagen"];
	if($valor != "")$files[] = UPLOADS_URL.$valor;		
	
	$res = split(",",$galeria);
	foreach($res as $valor){
		if($valor != "")$files[] = UPLOADS_URL.$valor;		
	}
	
}
foreach($arr as $valor){
	if($valor != "")$files[] = UPLOADS_URL.$valor;		
}



foreach($foo as $file){
	$files[] = ABS_URL.substr($file,2);	
}


$last = 0;
foreach($files as $file){
	if(substr($file,0,1) == "/"){
		$file = substr($file,strlen(ABS_URL));
	}
	$file = ABS_DIR.$file;
	
	$ts = filemtime($file);
	if($ts > $last)$last = $ts;
}
$totalsize = 0;
foreach($files as $file){
	if(substr($file,0,1) == "/"){
		$file = substr($file,strlen(ABS_URL));
	}
	$file = ABS_DIR.$file;
	
	$totalsize += filesize($file);
}
//Imprime archivo
echo "CACHE MANIFEST
#Version $last. Total size: "._format_bytes($totalsize)."
";


$i = 0;
foreach($files as $file){
	$f = substr($file,strlen(ABS_URL));
	//if($i++ >= 20)break;
	$i++;
	echo $i.": ".$f.": "._format_bytes(filesize($f))."\n";
	
}


function _format_bytes($a_bytes)
{
    if ($a_bytes < 1024) {
        return $a_bytes .' B';
    } elseif ($a_bytes < 1048576) {
        return round($a_bytes / 1024, 2) .' KiB';
    } elseif ($a_bytes < 1073741824) {
        return round($a_bytes / 1048576, 2) . ' MiB';
    } elseif ($a_bytes < 1099511627776) {
        return round($a_bytes / 1073741824, 2) . ' GiB';
    } elseif ($a_bytes < 1125899906842624) {
        return round($a_bytes / 1099511627776, 2) .' TiB';
    } elseif ($a_bytes < 1152921504606846976) {
        return round($a_bytes / 1125899906842624, 2) .' PiB';
    } elseif ($a_bytes < 1180591620717411303424) {
        return round($a_bytes / 1152921504606846976, 2) .' EiB';
    } elseif ($a_bytes < 1208925819614629174706176) {
        return round($a_bytes / 1180591620717411303424, 2) .' ZiB';
    } else {
        return round($a_bytes / 1208925819614629174706176, 2) .' YiB';
    }
}

?>