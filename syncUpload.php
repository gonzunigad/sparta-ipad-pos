<?php
$mainurl = "http://".$_SERVER['HTTP_HOST'].ABS_URL;
$page->put_additional_content = false;
if($_GET['type'] == "cotizacion"){
	if(count($_POST) <= 0)die("false");
	$mod = Model::instance("Cotizacion");
	foreach($_POST['nombre'] as $key => $valor){
		$cotizacion = array(
			"nombre" => $_POST['nombre'][$key],
			"direccion" => $_POST['direccion'][$key],
			"comuna" => $_POST['comuna'][$key],
			"ciudad" => $_POST['ciudad'][$key],
			"email" => $_POST['email'][$key],
			"rut" => str_replace(array(".",",","-"),"",$_POST['rut'][$key]),
			"genero" => $_POST['genero'][$key],
			"tienda_id" => $_POST['tienda_id'][$key],
			"subproducto" => $_POST['subproducto'][$key],
			"equipo_id" => $_POST['equipo_id'][$key]
		);
		$mod->insert($cotizacion);
	};
	
	
	$equipo = Model::instance("Producto");
	$producto = $equipo->getById($_POST['equipo_id'][$key]);
	
	$t = Model::instance("Tienda");
	$tienda = $t->getById($_POST['tienda_id'][$key]);
	
	$foto = "http://".$_SERVER['HTTP_HOST'].UPLOADS_URL.$producto["Producto"]["imagen"];
	
	$tmp = split("\n",$producto["Producto"]["detalle"]);
	$detalles = "";
	foreach($tmp as $valor){
		$detalles .= "<li>$valor</li>";
	}
	$detalles = "<ul>$detalles</ul>";
	
	$tmp = split("\n",$producto["Producto"]["caracteristicas"]);
	$caracteristicas = "";
	foreach($tmp as $valor){
		$caracteristicas .= "<li>$valor</li>";
	}
	$caracteristicas = "<ul>$caracteristicas</ul>";
	
	/************* ENVIO DE CORREO ***************/
	$para = $_POST["email"][$key];
	$subject = "Cotizacion de Compra Sparta: ".$producto["Producto"]["nombre"]." - ".$tienda["Tienda"]["nombre"];
	$headers = array(
		"BCC" => implode(",",$mails)
	);
	$array = array(
		// Cliente
		"nombre"						=> $_POST["nombre"][$key],
		
		//Producto
		"caracteristicas"			=> $caracteristicas,
		"detalles"						=> $detalles,
		"foto"								=> $foto,
		"precio"							=> $producto["Producto"]["precio"],
		"nombreproducto"		=> $producto["Producto"]["nombre"],
		"sku"								=> $_POST['subproducto'][$key],
		
		//Firma
		"jefetienda" 					=> $tienda["Tienda"]["jefe_tienda"],
		"nombretienda" 			=> $tienda["Tienda"]["nombre"],
		"telefonotienda" 			=> $tienda["Tienda"]["telefono"]
		
	);
	
	$template = "cotizacion-fitness";
	if($producto["Categoria"]["mail_template_cotizacion"] != "")$template = $producto["Categoria"]["mail_template_cotizacion"];
	
	
	sendMail($para,$subject,$template,$array,"Sparta <sparta@sparta.cl>",$headers);
	

	
	die("true");
}else{
	if(count($_POST) <= 0)die("false");
	$mod = Model::instance("Confirmacion");
	foreach($_POST['nombre'] as $key => $valor){
		$cotizacion = array(
			"nombre" => $_POST['nombre'][$key],
			"direccion" => $_POST['direccion'][$key],
			"comuna" => $_POST['comuna'][$key],
			"ciudad" => $_POST['ciudad'][$key],
			"email" => $_POST['email'][$key],
			"rut" => str_replace(array(".",",","-"),"",$_POST['rut'][$key]),
			"genero" => $_POST['genero'][$key],
			"equipo_id" => $_POST['equipo_id'][$key],
			"tipo_documento" => $_POST['tipo_documento'][$key],
			"numero_documento" => $_POST['numero_documento'][$key],
			"direccion_despacho" => $_POST['direccion_despacho'][$key],
			"razon_social" => $_POST['razon_social'][$key],
			"giro" => $_POST['giro'][$key],
			"rut_factura" => $_POST['rut_factura'][$key],
			"fecha_entrega" => $_POST['fecha_entrega'][$key],
			"telefono" => $_POST['telefono'][$key],
			"cantidad" => $_POST['cantidad'][$key],
		);
		$mod->insert($cotizacion);
	};
	
	
	
	
	$equipo = Model::instance("Producto");
	if($_POST['cantidad'][$key] == "")$_POST['cantidad'][$key] = 1;
	mysql_query("UPDATE productos as Producto SET  stock = stock-".$_POST['cantidad'][$key]." WHERE `Producto`.`id` = ".$_POST['equipo_id'][$key]);
	
	$producto = $equipo->getById($_POST['equipo_id'][$key]);
	
	$t = Model::instance("Tienda");
	$tienda = $t->getById($_POST['tienda_id'][$key]);
	
	if($producto["stock"] == 0){
		mysql_query("UPDATE productos as Producto SET  activo = false WHERE `Producto`.`id` = ".$_POST['equipo_id'][$key]);
	}
	set_config('lastchange',mktime());
	
	$ddesp = $_POST['direccion_despacho'][$key];
	if($ddesp == ''){
		$ddesp = "<ul><li>".$_POST["direccion"][$key]."</li><li>Ciudad: ".$_POST["ciudad"][$key]."</li><li>Comuna: ".$_POST["comuna"][$key]."</li>" ;
	}
	$foto = "http://".$_SERVER['HTTP_HOST'].UPLOADS_URL.$producto["Producto"]["imagen"];
	//
	
	
	$tmp = split("\n",$producto["Producto"]["detalle"]);
	$detalles = "";
	foreach($tmp as $valor){
		$detalles .= "<li>$valor</li>";
	}
	$detalles = "<ul>$detalles</ul>";
	
	$tmp = split("\n",$producto["Producto"]["caracteristicas"]);
	$caracteristicas = "";
	foreach($tmp as $valor){
		$caracteristicas .= "<li>$valor</li>";
	}
	$caracteristicas = "<ul>$caracteristicas</ul>";
	
	
	$para = $_POST["email"][$key];
	$subject = "Confirmacion de Compra Sparta: ".$producto["Producto"]["nombre"]." - ".$tienda["Tienda"]["nombre"];
	$message = "
	<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"> 
	<html xmlns:v=\"urn:schemas-microsoft-com:vml\">
	<head> 
		<style type=\"text/css\">
			v:* { behavior: url(#default#VML); display: inline-block; }
		</style>
	</head>
	<body>
	
	<body>
	<table width='693' height='1455' border='0' cellpadding='0' cellspacing='0'>
	  <tr>
		<td width='693' height='521'><div align='left'>
		  <table width='693' height='521'  border='0' cellpadding='0' cellspacing='0'>
			<tr>
			  <td height='215'><img src='".$mainurl."mail/mail_top_01.jpg' alt='mail_top' width='693' height='215'></td>
			</tr>
			<tr>
			  <td height='30' bgcolor='#000000'><div align='left' style='font-size: 18px;color: #FFFFFF;font-family: Geneva, Arial, Helvetica, sans-serif;font-weight: bold; margin-left:30px;'>".utf8_decode($_POST["nombre"][$key])."</div></td>
			</tr>
			<tr>
			  <td height='276' style='position:relative' BACKGROUND=\"".$mainurl."mail/mail_footer_confirmar.png\" style='background:url(\"".$mainurl."mail/mail_footer_confirmar.png\"); width:693px; height:276px;'>
				<!--[if gte vml 1]>
				<v:image style='width: 693px; height: 276px; position: absolute; top: 0; left: 0; border: 0; z-index: -1;' src=\"".$mainurl."mail/mail_footer_confirmar.png\" />
				<![endif]-->
	
				<table><tr><td><div style='height:182px'><img src='".$mainurl."mail/pixel-182.gif' height='182' width='16' /></div></td></tr>
				<tr>
				<td><div style='width:16px'><img src='".$mainurl."mail/pixel.gif' width='16' /></div></td>
				<td>
					<div style='color:#fff; font-family:verdana, sans-serif; font-size:12px; line-height:15px;'><strong>".utf8_decode($tienda["Tienda"]["jefe_tienda"])."</strong><br/>Jefe Tienda Sparta<br />".utf8_decode($tienda["Tienda"]["nombre"])."<br />".utf8_decode($tienda["Tienda"]["telefono"])."</div>
				</td></tr></table>
				<!--<img src='' alt='mail_top04' width='693' height='276'>-->
			</td>
			</tr>
		  </table>
		</div></td>
	  </tr>
	  <tr>
		<td width='693' height='449'><table width='693' height='736' border='0' cellpadding='0' cellspacing='0'>
		  <tr>
			<td width='321' height='425'><img src='".$mainurl."mail/mail_contacto.png' alt='texto'></td>
			<td width='372' height='425'><table width='372' height='424' border='0' cellspacing='0' cellpadding='0'>
			  <tr>
				<td width='152' height='173' valign='middle'><div style='font-size: 24px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>".$producto["Producto"]["nombre"]."</div></td>
				<td width='220'><div align='center'><img src='".$foto."' alt='Producto' width='170' height='170'></div></td>
			  </tr>
			  <tr>
				<td height='220' colspan='2' valign='top' background='http://sparta.resetpixel.com/mail/mail_general.jpg' bgcolor='#CACACA'><table width='100%' border='0' cellspacing='5'>
				  <tr>
					<td valign='top'>             
					<div align='left' style='font-size: 12px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>
					<p style='font-size: 14px; margin:0; padding:0;display:inline;'>Caracteristicas Generales:</p>".$caracteristicas." 
					</div></td>
				  </tr>
				  
				</table></td>
				</tr>
			</table></td>
		  </tr>
		  <tr>
			<td height='311' colspan='2'><table width='692' height='282' border='0' cellspacing='5' cellpadding='0'>
			  <tr>
				<td width='388' height='20' bgcolor='#00b9ff'><div align='left' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>Descripcion de Articulo:</div></td>
				<td width='93' height='20' bgcolor='#00b9ff'><div align='left' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>Cantidad:</div></td>
				<td width='102' height='20' bgcolor='#00b9ff'><div align='left' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>Precio Unitario:</div></td>
				<td width='84' bgcolor='#00b9ff'><div align='left' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>Precio:</div></td>
			  </tr>
			  <tr>
				<td height='247' valign='top' bgcolor='#dfdfdf'><div align='left' style='font-size: 12px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>
			 ".$detalles."
				</div></td>
				<td valign='top' bgcolor='#dfdfdf'><div align='right' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>".$_POST["cantidad"][$key]."</div></td>
				<td valign='top' bgcolor='#dfdfdf'><div align='right' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>$ ".number_format($producto["Producto"]["precio"],0,'.','.')."</div></td>
				<td valign='top' bgcolor='#dfdfdf'><div align='right' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'> $ ".number_format($producto["Producto"]["precio"]*$_POST["cantidad"][$key],0,'.','.')."</div></td>
			  </tr>
			</table>
			<table width='692' height='190' border='0' cellspacing='5' cellpadding='0'>
			  <tr>
				<td width='270' height='20' bgcolor='#00b9ff'><div align='left' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>Datos de Contacto:</div></td>
				<td width='261' height='20' bgcolor='#00b9ff'><div align='left' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>Direccion de Despacho:</div></td>
				<td width='141' bgcolor='#00b9ff'><div align='left' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>Fecha Entrega:</div></td>
			  </tr>
			  <tr>
				<td height='140' valign='top' bgcolor='#dfdfdf'><div align='left' style='font-size: 12px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'><ul>
				<li>".$_POST["tipo_documento"][$key]." : ".$_POST["numero_documento"][$key]."</li>
				<li>Nombre: ".utf8_decode($_POST["nombre"][$key])."</li>
				<li>Direccion: ".utf8_decode($_POST["direccion"][$key])."</li>
				<li>Comuna: ".utf8_decode($_POST["comuna"][$key])."</li>
				<li>Ciudad: ".utf8_decode($_POST["ciudad"][$key])."</li>
				<li>E-mail: ".$_POST["email"][$key]."</li>
				<li>Telefono: ".$_POST["telefono"][$key]."</li>
				<li>Rut: ".$_POST["rut"][$key]."</li>
				<li>----------------------------------------</li>
				<li>Razon Social: ".utf8_decode($_POST["razon_social"][$key])."</li>
				<li>Giro: ".utf8_decode($_POST["giro"][$key])."</li>
				<li>Rut: ".$_POST["rut_factura"][$key]."</li>
				</ul>
				</div>			
				</td>			
				<td valign='top' bgcolor='#dfdfdf'><div align='left' style='font-size: 14px;font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'> ".utf8_decode($ddesp)."</div></td>
				<td valign='top' bgcolor='#dfdfdf'><div align='left' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>
				  <div align='right'> ".$_POST["fecha_entrega"][$key]."</div>
				</div></td>
			  </tr>
			</table>
			</td>
			</tr>
		</table></td>
	  </tr>
	  <!--<tr>
		<td width='693' height='485'><a href='http://www.sparta.cl'><img src='http://sparta.resetpixel.com/mail/mail_botton_confirmacion.jpg' alt='sparta' width='693' height='485' border='0'></a></td>
	  </tr>-->
	</table>
	</body>
	</html>
	";
	
	 
	// Para enviar un correo HTML mail, la cabecera Content-type debe fijarse
	$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
	$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	
	// Cabeceras adicionales
	$mails = array_merge( split("\n",get_config("mails_confirmacion")) , split("\n",$tienda["Tienda"]["mails_confirmacion"]) );
	//$mails = array("gonzunigad@gmail.com");
	$cabeceras .= 'From: Sparta <sparta@sparta.cl>' . "\r\n";
	$cabeceras .= 'Bcc: '. implode(",",$mails);
	
	 
	mail($para, $subject, $message, $cabeceras);
	
	
	die("true");
}
?>