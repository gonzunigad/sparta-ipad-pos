<?php
$page->template = false;
$mod = Model::instance("Cotizacion");
$cdata = $mod->getById($_GET['id']);

$equipo = Model::instance("Producto");
$producto = $equipo->getById($cdata['equipo_id']);

$t = Model::instance("Tienda");
$tienda = $t->getById($cdata['tienda_id']);

$foto = "http://".$_SERVER['HTTP_HOST'].UPLOADS_URL.$producto["Producto"]["imagen"];

$tmp = split("\n",$producto["Producto"]["detalle"]);
$detalles = "";
foreach($tmp as $valor){
	$detalles .= "<li>$valor</li>";
}
$detalles = "<ul>$detalles</ul>";

$tmp = split("\n",$producto["Producto"]["caracteristicas"]);
$caracteristicas = "";
foreach($tmp as $valor){
	$caracteristicas .= "<li>$valor</li>";
}
$caracteristicas = "<ul>$caracteristicas</ul>";

/************* ENVIO DE CORREO ***************/
$para = $cdata["email"];
$para = "gonzunigad@gmail.com";
$subject = "Cotizacion de Compra Sparta: ".$producto["Producto"]["nombre"]." - ".$tienda["Tienda"]["nombre"];
$headers = array(
	//"BCC" => implode(",",$mails)
);
$array = array(
	// Cliente
	"nombre"					=> $cdata["nombre"],
	
	//Producto
	"caracteristicas"			=> $caracteristicas,
	"detalles"					=> $detalles,
	"foto"						=> $foto,
	"precio"					=> $producto["Producto"]["precio"],
	"nombreproducto"			=> $producto["Producto"]["nombre"],
	"sku"						=> $cdata['subproducto'],
	
	//Firma
	"jefetienda" 				=> $tienda["Tienda"]["jefe_tienda"],
	"nombretienda" 				=> $tienda["Tienda"]["nombre"],
	"telefonotienda" 			=> $tienda["Tienda"]["telefono"]
	
);

$template = "cotizacion-fitness";
if($producto["Categoria"]["mail_template_cotizacion"] != "")$template = $producto["Categoria"]["mail_template_cotizacion"];

echo sendMail($para,$subject,$template,$array,"Sparta <sparta@sparta.cl>",$headers,true,false);
?>